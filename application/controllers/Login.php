<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {

/*session is started if you don't write this line can't use $_Session  global variable*/

        parent::__construct();
        $this->load->model('login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {

        if($this->session->customerLoggedIn==1) {
            redirect('/profile/dashboard/billing');
        }

        if($this->input->post())
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            $result = $this->login_model->loginMe($email, $password);

           if($result=="") {
            echo "<script>alert('Please enter valid username');</script>";
            redirect('/login');
                        exit;

           }


                                   $sessionArray = array('id'=>$result->id,
                                        'name'=>$result->name,
                                        'customerLoggedIn' => TRUE
                                );
                $this->session->set_userdata($sessionArray);



            redirect('/profile/dashboard/checkout');

        }
        $data['name'] = 'asdf';

        $this->loadViews('login/index',$this->global,$data,NULL);
     
    }
}
?>