<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Exam Name</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Name details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $examName->name; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Descrption <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $examName->description; ?>">
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_marks" name="total_marks" placeholder="Total Marks" value="<?php echo $examName->total_marks; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Question <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_question" name="total_question" placeholder="Total Question" value="<?php echo $examName->total_question; ?>">
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Total Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="total_time" name="total_time" placeholder="Total Time" value="<?php echo $examName->total_time; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Min. Marks <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="min_marks" name="min_marks" placeholder="Min. Marks" value="<?php echo $examName->min_marks; ?>">
                        </div>
                      </div>
                    </div>

                </div>
   

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Display Result Immediately <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioResult1" name="display_result_immediately" class="custom-control-input" value="0" 
                            <?php if($examName->display_result_immediately==0) {
                                 echo "checked=checked";
                              };?>
                            >
                            <label class="custom-control-label" for="customRadioResult1">No</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioResult2" name="display_result_immediately" class="custom-control-input" value="1" 
                            <?php if($examName->display_result_immediately==1) {
                                 echo "checked=checked";
                              };?>
                            >
                            <label class="custom-control-label" for="customRadioResult2">Yes</label>
                          </div>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" 
                            <?php if($examName->status==1) {
                                 echo "checked=checked";
                              };?>
                            >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($examName->status==0) {
                                 echo "checked=checked";
                              };?>
                            >
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div> 




                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

     $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                description: {
                    required: true
                },
                total_marks: {
                    required: true
                },
                total_question: {
                    required: true
                },
                total_time: {
                    required: true
                },
                min_marks: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                description: {
                    required: "<p class='error-text'>Description Required</p>",
                },
                total_marks: {
                    required: "<p class='error-text'>Total Marks Required</p>",
                },
                total_question: {
                    required: "<p class='error-text'>Total Question Required</p>",
                },
                total_time: {
                    required: "<p class='error-text'>Total Time Required</p>",
                },
                min_marks: {
                    required: "<p class='error-text'>Min Marks Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">

    function reloadPage()
    {
      window.location.reload();
    }
</script>