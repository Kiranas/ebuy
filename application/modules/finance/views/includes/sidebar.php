 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('currency.list','currency.add','currency.edit','currency_rate_setup.list','currency_rate_setup.add'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Finance Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('currency.list','currency.add','currency.edit','currency_rate_setup.list','currency_rate_setup.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/finance/currency/list" class="nav-link <?php if(in_array($pageCode,array('currency.list','currency.add','currency.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Pending Invoice</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/finance/currencyRateSetup/list" class="nav-link <?php if(in_array($pageCode,array('currency_rate_setup.list','currency_rate_setup.add','currency_rate_setup.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Completed Invoice</span>                      
                    </a>
                  </li>

                </ul>
              </li>  
              
                         
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>