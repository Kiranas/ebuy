<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class StudentRegistration extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('student.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            // $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 
            $data['searchParam'] = $formData;

            $data['studentList'] = $this->student_model->studentListSearch($formData);
            // $data['locationList'] = $this->student_model->studentLocationList();

            // echo "<Pre>";print_r($data);exit();
            $this->global['pageCode'] = 'student.list';
            $this->global['pageTitle'] = 'Campus Management System : Student List';
            
            $this->loadViews("student_view/list", $this->global, $data, NULL);
        }
    }
    function studentexams($id = NULL)
    {
        if ($this->checkAccess('student.studentexams') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            // $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));

            $data['examList'] = $this->student_model->studentExams($id);
            // $data['locationList'] = $this->student_model->studentLocationList();

            // echo "<Pre>";print_r($data);exit();
            $this->global['pageCode'] = 'student.studentexams';
            $this->global['pageTitle'] = 'Campus Management System : Student Exams';
            
            $this->loadViews("student_view/studentexams", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('student.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;





                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $permanent_country = $this->security->xss_clean($this->input->post('id_country'));
                $permanent_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $exam_type = $this->security->xss_clean($this->input->post('exam_type'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $nric = $this->security->xss_clean($this->input->post('nric'));


                $salutation_data = $this->student_model->getSalutation($salutation);


                $data = array(
                   'salutation' => $salutation,
                   'first_name' => $first_name,
                   'last_name' => $last_name,
                   'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $city,
                    'permanent_zipcode' => $zipcode,
                    'permanent_address1' => $address,
                    'email_id' => $email,
                    'password' => $password,
                    'nric' => $nric,
                    'status' => 1,
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit();

                $result = $this->student_model->addStudent($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Center created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Center creation failed');
                }
                redirect('/student/studentRegistration/list');
            }

            $data['countryList'] = $this->student_model->countryListByStatus('1');
            $data['stateList'] = $this->student_model->stateListByStatus('1');
            $data['salutationList'] = $this->student_model->salutationListByStatus('1');
            $data['tosList'] = $this->student_model->tosListByStatus('1');


            $this->global['pageCode'] = 'student.add';
            $this->global['pageTitle'] = 'Campus Management System : Student Registration';
            $this->loadViews("student_view/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('student.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/student/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;






                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $permanent_country = $this->security->xss_clean($this->input->post('id_country'));
                $permanent_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $exam_type = $this->security->xss_clean($this->input->post('exam_type'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $nric = $this->security->xss_clean($this->input->post('nric'));


                $salutation = $this->student_model->getSalutation($salutation);


                $data = array(
                   'salutation' => $salutation,
                   'first_name' => $first_name,
                   'last_name' => $last_name,
                   'full_name' => $salutation->name . ". " . $first_name . " " . $last_name,
                    'permanent_country' => $permanent_country,
                    'permanent_state' => $permanent_state,
                    'permanent_city' => $city,
                    'permanent_zipcode' => $zipcode,
                    'permanent_address1' => $address,
                    'email_id' => $email,
                    'password' => $password,
                    'nric' => $nric,
                    'id_tos' => $id_tos,
                    'contact_person' => $contact_person,
                    'contact_number' => $contact_number,
                    'id_location' => $id_location,
                    'status' => 0,
                    'created_by' => $user_id
                );

                $result = $this->student_model->editStudent($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Student Registered Successfully');
                } else {
                    $this->session->set_flashdata('error', 'Student Registration Failed');
                }
                redirect('/exam/student/list');
            }
            $data['countryList'] = $this->student_model->countryListByStatus('1');
            $data['stateList'] = $this->student_model->stateListByStatus('1');
            $data['tosList'] = $this->student_model->tosListByStatus('1');
            
            $data['getStudentList'] = $this->student_model->getStudentList($id);
            $this->global['pageCode'] = 'student.edit';
            $this->global['pageTitle'] = 'Campus Management System : Edit Student Registration';
            $this->loadViews("student_view/edit", $this->global, $data, NULL);
        }
    }


    function addRoomCapacity($id = NULL)
    {
        if ($this->checkAccess('student.room_capacity_add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/student/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $room = $this->security->xss_clean($this->input->post('room'));
                $capacity = $this->security->xss_clean($this->input->post('capacity'));


                
                $data = array(
                    'id_student' => $id,
                    'room' => $room,
                    'capacity' => $capacity,
                    'status' => 1,
                    'created_by' => $user_id
                );

                $result = $this->student_model->addStudentHasRoomCapacity($data);
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Center edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Center edit failed');
                }
                redirect('/exam/student/addRoomCapacity/'.$id);
            }
            $data['countryList'] = $this->student_model->countryListByStatus('1');
            $data['stateList'] = $this->student_model->stateListByStatus('1');
            $data['locationList'] = $this->student_model->studentLocationListByStatus('1');
            $data['tosList'] = $this->student_model->tosListByStatus('1');

            
            
            $data['getStudentList'] = $this->student_model->getStudentList($id);
            $data['centerHasRoomCapacity'] = $this->student_model->getStudentHasRoom($id);
            

            $this->global['pageTitle'] = 'Campus Management System : Add Room Capacity Exam Center';
            $this->global['pageCode'] = 'student.edit';
            $this->loadViews("student/add_room_capacity", $this->global, $data, NULL);
        }
    }


    function getStateByCountry($id_country)
    {
            $results = $this->student_model->getStateByCountryId($id_country);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }

    function deleteRoomCapacity($id)
    {
        $inserted_id = $this->student_model->deleteRoomCapacity($id);
        echo "Success";
    }
}
