<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Login</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/css/main.css" rel="stylesheet" />
  </head>
  <body>
    <div class="login-wrapper">
      <div class="container-fluid">
        <form action="" method="post">
        <div class="row">
          <div class="col-sm-8 col-lg-5 col-xl-4">
            <div class="login-container d-flex align-items-center">
              <div>
                <h1>Speed Portal</h1>
                <h3>Welcome To Admin Speed Portal</h3>
                <div class="form-group">
                  <label for="email">Email address</label>
                  <input type="email" class="form-control" id="email" name="email"  placeholder="name@example.com">
                </div>   
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password"  placeholder="Password">
                </div>   
                
                <button type="submit" class="btn btn-primary btn-lg btn-block my-4">Sign In</button>  
                <!-- <p><a href="#">Student Portal User Manual</a></p> 
                <div class="d-flex">
                  <p>New User? <a href="#">Register Now!</a></p>
                  <p class="ml-auto"><a href="#">Forgot Password?</a></p>
                </div>  -->                                                      
              </div>
            </div>
          </div>
        </div>
        </form>
      </div>
    </div>
      <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
          <script src="<?php echo BASE_PATH; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_PATH; ?>assets/js/main.js"></script>
  </body>
</html>