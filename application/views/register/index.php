
<div class="py-2 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Register</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Register</a>
        </li>
       
      </ol>
     </div>
   </div>
  </div> 
</div>




<form method="POST" action="<?php echo base_url(); ?>register/index">

<section class="padding-y-10">
  <div class="container">
   <div class="row">
        <div class="col-md-12 order-md-1">
          <h4 class="mb-3">New Registration Details</h4>
            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name='name' placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid Name is required.
                </div>
              </div>
               <div class="col-md-6 mb-3">
                <label for="firstName">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="" required="required" onblur="checkduplicate()">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="name">Mobile</label>
                <input type="text" class="form-control" id="mobile" name='mobile' placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid Mobile Number is required.
                </div>
              </div>
               <div class="col-md-6 mb-3">
                <label for="firstName">Date of Birth</label>
                <input type="text" class="form-control" id="dob" name="dob" placeholder="" value="" required="required" >
                <div class="invalid-feedback">
                  Valid Date of Birth is required.
                </div>
              </div>
            </div>
          

             <div class="row">
              
              <div class="col-md-6 mb-3">
                <label for="lastName">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
               <div class="col-md-6 mb-3">
                <label for="lastName">Confirm Password</label>
                <input type="password" class="form-control" id="confirm-password" name="confirm-password" placeholder="" value="" required="">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>


            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Continue </button>
        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>



<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script>
  $( function() {
    $( "#dob" ).datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: "-80:+0",
    });
  } );
  </script>
<script>
function checkduplicate() {
  var email = $("#email").val();
        $.get("/register/duplicateemail/"+email, function(data, status){
             if(data=='1') {
              alert("Email already exist please login and proceed further");
               $("#email").val(' ');
             }
         });
}
</script>
  </body>
</html>