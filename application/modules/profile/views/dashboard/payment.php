<style>
.razorpay-payment-button {
  display: none;
}
</style>
<form action="success" method="POST">
  <script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="<?php echo $checkoutdata['key']?>"
    data-amount="<?php echo $checkoutdata['amount']?>"
    data-currency="INR"
    data-name="<?php echo $checkoutdata['name']?>"
    data-image="<?php echo $checkoutdata['image']?>"
    data-description="<?php echo $checkoutdata['description']?>"
    data-prefill.name="<?php echo $checkoutdata['prefill']['name']?>"
    data-prefill.email="<?php echo $checkoutdata['prefill']['email']?>"
    data-prefill.contact="<?php echo $checkoutdata['prefill']['contact']?>"
    data-notes.shopping_order_id="3456"
    data-order_id="<?php echo $checkoutdata['order_id']?>"
    <?php if ($displayCurrency !== 'INR') { ?> data-display_amount="<?php echo $checkoutdata['display_amount']?>" <?php } ?>
    <?php if ($displayCurrency !== 'INR') { ?> data-display_currency="<?php echo $checkoutdata['display_currency']?>" <?php } ?>
  >
  </script>
  <!-- Any extra fields to be submitted with the form but not sent to Razorpay -->
  <input type="hidden" name="shopping_order_id" value="3456">





<div class="py-2 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Checkout </h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Checkout </a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>


 
  <section class="padding-y-10">
  <div class="container">
   <div class="row">
    
     <div class="col-12">
       <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Category Name</th>
            <th scope="col">Course</th>
            <th scope="col">Amount</th>
                        <th scope="col">Quantity</th>

            <th scope="col">Total</th>
          </tr>
        </thead>
        <tbody>

<?php $finaltotal = 0;
$productamount = 0;
for($i=0;$i<count($listOfCourses);$i++) {

  $productamount = $listOfCourses[$i]->quantity * $listOfCourses[$i]->amount;


$finaltotal = $finaltotal + $productamount;
 ?>
          <tr>
            <td class="p-4">
            <span class="d-inline-block width-7rem border p-3 mr-3">
             <img src="<?php echo BASE_PATH;?>website/images/<?php echo $listOfCourses[$i]->image;?>" alt="">
            </span>
              <a href="#"><br/><?php echo $listOfCourses[$i]->categoryname;?></a>
            </td>
            
            <td class="p-4">
              <span class="d-inline-block width-7rem border p-3 mr-3">
             <img src="<?php echo BASE_PATH;?>website/images/<?php echo $listOfCourses[$i]->file;?>" alt="">
            </span>
              <a href="#"><br/><?php echo $listOfCourses[$i]->coursename;?></a>
            </td>
            <td class="text-center">
              <?php echo $listOfCourses[$i]->amount;?>
            

            </td>
             <td class="text-center">
              <?php echo $listOfCourses[$i]->quantity;?>
            

            </td> <td class="text-center">
              <?php echo $productamount;?>
            

            </td>


          </tr>

<?php } ?>
          <tr>
          <td colspan="5" style="text-align: right;">
            Total: <span class="font-weight-semiBold font-size-18"><?php echo $finaltotal;?></span>
          </td>
          </tr>
        </tbody>
      </table>
      </div>
     </div> <!-- END col-12 -->
     
    
     <div class="col-md-6 mt-4 text-right">
      <?php if($finaltotal>0) { ?>
       <button type='submit' class="btn btn-primary ml-3">Pay Now</button>
     <?php } ?>
     </div> <!-- END col-md-6 -->
   </div> <!-- END row-->  
  </div> <!-- END container-->
</section>




</form>