<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Role extends BaseController
{
    public function __construct()
    {
        // echo "P";exit;
        parent::__construct();
        $this->load->model('role_model');
        $this->load->model('permission_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('role.list') == 0)
        {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['roleList'] = $this->role_model->roleListSearch($formData);
            $this->global['pageTitle'] = 'Examination Management System : Role List';
            $this->global['pageCode'] = 'role.list';
            $this->loadViews("role/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('role.add') == 0)
        {
            $this->loadAccessRestricted();
        } else {

            if ($this->input->post()) {

                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $permissions = $this->input->post('permissions');
                $data = array(
                    'role' => $role,
                    'status' => $status
                );
 
                $result = $this->role_model->addNewRole($data);
                $this->role_model->updateRolePermissions($permissions, $result);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Role created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Role creation failed');
                }
                redirect('/setup/role/list');
            }
            $data['permissions'] = $this->permission_model->permissionListSearch(null);
            $this->global['pageTitle'] = 'Examination Management System : Add Role';
            $this->global['pageCode'] = 'role.add';
            $this->loadViews("role/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('role.edit') == 0)
        {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/role/list');
            }
            if ($this->input->post()) {
                $role = $this->security->xss_clean($this->input->post('role'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $permissions = $this->input->post('permissions');

                $data = array(
                    'role' => $role,
                    'status' => $status
                );

                $result = $this->role_model->editRole($data, $id);

                if ($result) {
                    $this->role_model->updateRolePermissions($permissions, $id);
                    $this->session->set_flashdata('success', 'Role edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Role edit failed');
                }
                redirect('/setup/role/list');
            }

            $data['role'] = $this->role_model->getRole($id);
            $data['rolepermissions'] = $this->role_model->rolePermissions($id);
            $data['permissions'] = $this->permission_model->permissionListSearch(null);
            $this->global['pageTitle'] = 'Examination Management System : Edit Role';
            $this->global['pageCode'] = 'role.edit';
            $this->loadViews("role/edit", $this->global, $data, NULL);
        }
    }
}
