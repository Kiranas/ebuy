<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Product extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');

        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index($id)
    {

$data['categoryDetails'] = $this->register_model->getCategoryById($id);
$data['url_name'] = $id;



                $this->loadViews('product/index',$this->global,$data,NULL);

        
    }


    public function getproductlist() {
        $category = 0;
        if($_POST) {
            for($k=0;$k<count($_POST['categories']);$k++) {
                $category = $category.','.$_POST['categories'][$k];
            }
         }
        else {
            $category = "1,2,3";
        }

$productList = $this->register_model->getAllCourses($category);

        $table="<div class='row'>";
        for($l=0;$l<count($productList);$l++) {
            $productname = $productList[$l]->name;
            $file = BASE_PATH."assets/images/".$productList[$l]->file;
            $price = $productList[$l]->price;
            $id = $productList[$l]->id;
            $productname = $productList[$l]->name;
            $productname = $productList[$l]->name;
            $productname = $productList[$l]->name;
            $productname = $productList[$l]->name;
            // print_r($productList[$l]);
            $detailsurl = BASE_PATH."coursedetails/index/".$productList[$l]->id;

          $table.="<div class='col-lg-4 col-md-6 marginTop-30 wow fadeIn'>
            <div class='card text-center height-100p shadow-v1'>
              <div class='card-header'>
                <img class='w-100' src='$file' alt=''>
              </div>
              <div class='card-body px-3 py-0'>
               <a href='$detailsurl' class='h6'>$productname</a>
               <p class='text-gray'>
                 Ezzibuy
               </p>
               <div class='d-flex align-items-center justify-content-between'>
                 <p class='mb-0 font-weight-semiBold'>
                   <span class='text-primary'>&#x20b9; $price</span>
                 </p>
                <ul class='list-unstyled ec-review-rating font-size-12'>
                  <li class='active'><i class='fas fa-star'></i></li>
                  <li class='active'><i class='fas fa-star'></i></li>
                  <li class='active'><i class='fas fa-star'></i></li>
                  <li class='active'><i class='far fa-star'></i></li>
                  <li class='active'><i class='far fa-star'></i></li>
                </ul>
               </div>
              </div>
              <div class='card-footer border-top-0'>
                <button class='btn btn-outline-primary mx-1' onclick='buynow($id,$price)'>Buy Now</button>
                <button class='btn btn-outline-light mx-1'         
                  data-container='body'
                  data-toggle='tooltip'
                  data-placement='top'
                  data-skin='light'
                  title='Add to card' onclick='buynow($id,$price)'>
                  <i class='ti-shopping-cart'></i>
                 </button>
              </div>
            </div>
            </div>";
           } 
          $table.="</div><!-- END col-md-9-->";
      echo $table;
    }

    
}