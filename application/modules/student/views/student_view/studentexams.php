<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Exams</h1>
    <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Student</a> -->
  </div>
  <div class="page-container">
    
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Name</th>
            <th>Exam Set</th>
            <th>Date</th>
            <th>Correct</th>
            <th>Wrong</th>
            <th>Grade</th>
            <th class="text-center">Status</th>
            <!-- <th style="text-align: center;">Action</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examList)) {
            $i = 1;
            foreach ($examList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->full_name; ?></td>
                <td><?php echo $record->examset_code; ?></td>
                <td><?php echo $record->exam_date; ?></td>
                <td><?php echo $record->correct; ?></td>
                <td><?php echo $record->wrong; ?></td>
                <td><?php echo $record->grade; ?></td>
                <td style="text-align: center;"><?php if ($record->status == '1') {
                  echo "Finished";
                } else if ($record->status == '2') {
                  echo "Failed";
                }
                else{
                  echo "Ongoing";
                }
                ?>
                </td>
                <!-- <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="button" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>

                </td> -->
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>