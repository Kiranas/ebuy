<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class ExamCenterLogin extends BaseController
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_login_model');
    }

    public function index()
    {
        // echo "<Pre>";print_r("dasdsa");exit();
        $this->checkExamCenterLoggedIn();
    }
    

    function checkExamCenterLoggedIn()
    {
        $isExamCenterLoggedIn = $this->session->userdata('isExamCenterLoggedIn');
        
        if(!isset($isExamCenterLoggedIn) || $isExamCenterLoggedIn != TRUE)
        {
            $this->load->view('exam_center_login');
        }
        else
        {
            echo "Login";exit();
            redirect('scholarship/scheme/list');
        }
    }


    public function loginMe()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->exam_center_login_model->loginExamCenter($email, $password);
            
            
            if(!empty($result))
            {
                
                    
                $lastLogin = $this->exam_center_login_model->examCenterLastLoginInfo($result->id);

                if($lastLogin == '')
                {
                    $exam_center_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $exam_center_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_exam_center'=>$result->id,
                                        'exam_center_name'=>$result->name,
                                        'id_exam_center_location'=>$result->id_location,
                                        'exam_center_last_login'=> $exam_center_login,
                                        'isExamCenterLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_exam_center'], $sessionArray['isExamCenterLoggedIn'], $sessionArray['exam_center_last_login']);

                $loginInfo = array("id_exam_center"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("exam_center_session_id", md5($uniqueId));


                $this->exam_center_login_model->addExamCenterLastLogin($loginInfo);

                echo "Login";exit();
                // echo md5($uniqueId);exit();
                // redirect('/scholarship/scheme/list');
                }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }
}

?>