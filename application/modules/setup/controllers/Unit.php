<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Unit extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('unit_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('unit.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));

            $data['searchParam'] = $formData;
            $data['unitList'] = $this->unit_model->unitListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Unit List';
            $this->global['pageCode'] = 'unit.list';
            
            $this->loadViews("unit/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('unit.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // if($_FILES['file']['name'])
                // {


                // $certificate_name = $_FILES['file']['name'];
                // $certificate_size = $_FILES['file']['size'];
                // $certificate_tmp =$_FILES['file']['tmp_name'];
                
                // // echo "<Pre>"; print_r($certificate_tmp);exit();

                // $certificate_ext=explode('.',$certificate_name);
                // $certificate_ext=end($certificate_ext);
                // $certificate_ext=strtolower($certificate_ext);


                // $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'File');

                // $file = $this->uploadFile($certificate_name,$certificate_tmp,'File');


                // }


            
                $name = $this->security->xss_clean($this->input->post('name'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_category' => $id_category,
                    'id_course' => $id_course,
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );

                // if($file)
                // {
                //     $data['file'] = $file;
                // }
                
                $result = $this->unit_model->addNewUnit($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Unit created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Unit creation failed');
                }
                redirect('/setup/unit/list');
            }
            
            $data['categoryList'] = $this->unit_model->categoryListByStatus('1');
           
            $this->global['pageCode'] = 'unit.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Unit';
            $this->loadViews("unit/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('unit.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/unit/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                // if($_FILES['file']['name'])
                // {


                // $certificate_name = $_FILES['file']['name'];
                // $certificate_size = $_FILES['file']['size'];
                // $certificate_tmp =$_FILES['file']['tmp_name'];
                
                // // echo "<Pre>"; print_r($certificate_tmp);exit();

                // $certificate_ext=explode('.',$certificate_name);
                // $certificate_ext=end($certificate_ext);
                // $certificate_ext=strtolower($certificate_ext);


                // $this->fileFormatNSizeValidation($certificate_ext,$certificate_size,'File');

                // $file = $this->uploadFile($certificate_name,$certificate_tmp,'File');


                // }



                $name = $this->security->xss_clean($this->input->post('name'));
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'id_category' => $id_category,
                    'id_course' => $id_course,
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_by' => $user_id
                );
                
                // if($file)
                // {
                //     $data['file'] = $file;
                // }


                $result = $this->unit_model->editUnit($data,$id);
                if ($result)
                {
                    $this->session->set_flashdata('success', 'Unit edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Unit edit failed');
                }
                redirect('/setup/unit/list');
            }
            $data['categoryList'] = $this->unit_model->categoryListByStatus('1');

            $data['unit'] = $this->unit_model->getUnit($id);

            $this->global['pageCode'] = 'unit.list';
            $this->global['pageTitle'] = 'Examination Management System : Edit Unit';
            
            $this->loadViews("unit/edit", $this->global, $data, NULL);
        }
    }

    function markDistribution($id)
    {
        if ($this->checkAccess('unit.marks_distribution') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/unit/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit;

                $module = $this->security->xss_clean($this->input->post('module'));
                $max_marks = $this->security->xss_clean($this->input->post('max_marks'));
                $pass_marks = $this->security->xss_clean($this->input->post('pass_marks'));
                $is_pass_compulsary = $this->security->xss_clean($this->input->post('is_pass_compulsary'));
            
                $data = array(
                    'id_unit' => $id,
                    'module' => $module,
                    'max_marks' => $max_marks,
                    'pass_marks' => $pass_marks,
                    'is_pass_compulsary' => $is_pass_compulsary,
                    'status' => 1,
                    'created_by' => $user_id
                );


                $result = $this->unit_model->addMarksDistribution($data);
                if ($result)
                {
                    $this->session->set_flashdata('success', 'Marks Distribution Added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Marks Distribution Add failed');
                }
                redirect('/setup/unit/markDistribution/'.$id);
            }

            $data['categoryList'] = $this->unit_model->categoryListByStatus('1');

            $data['unit'] = $this->unit_model->getUnit($id);
            $data['unitMarksDistribution'] = $this->unit_model->unitMarksDistribution($id);

            $this->global['pageTitle'] = 'Examination Management System : Add Mark Distribution To Unit';
            $this->global['pageCode'] = 'unit.marks_distribution';
            
            $this->loadViews("unit/marks_distribution", $this->global, $data, NULL);
        }
    }

    function getCourseByCategory($id)
    {
        $results = $this->unit_model->getCourseByCategory($id);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        // <select name='id_course' id='id_course' class='form-control' onchange='getFeeStructureByData()'>
        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_course' id='id_course' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $code = $results[$i]->code;
        $table.="<option value=" . $id . ">" . $code . " - " . $name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function deleteMarksDistribution($id)
    {
        $results = $this->unit_model->deleteMarksDistribution($id);
        echo "sucess";exit();
    }
}
