 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
            
              
              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('exam_name.list','exam_name.add','exam_name.edit','exam_center.list','exam_center.add','exam_center.edit','exam_event.list','exam_event.add','exam_event.edit','exam_has_question.list','exam_has_question.add','exam_has_question.edit','grade.list','grade.add','grade.edit','location.list','location.add','location.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>Student</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('student.list','student.add','student.edit','exam_student_tagging.list','exam_student_tagging.add','exam_student_tagging.edit','student.studentexams'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">

                  <li class="nav-item">
                    <a href="/student/studentRegistration/list" class="nav-link <?php if(in_array($pageCode,array('student.list','student.edit','student.add','student.studentexams'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Student Registration</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/student/examStudentTagging/list" class="nav-link <?php if(in_array($pageCode,array('exam_student_tagging.list','exam_student_tagging.edit','exam_student_tagging.add'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Student Exam Tagging</span>                      
                    </a>
                  </li>

                </ul>
              </li>                
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>