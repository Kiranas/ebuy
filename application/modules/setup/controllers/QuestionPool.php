<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class QuestionPool extends BaseController
{
    public function __construct()
    { 
        parent::__construct();
        $this->load->model('question_pool_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('questionpool.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['code'] = $this->security->xss_clean($this->input->post('code'));

            $data['searchParam'] = $formData;
            $data['poolList'] = $this->question_pool_model->poolListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Question Pool List';
            $this->global['pageCode'] = 'questionpool.list';
            $this->loadViews("questionpool/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('questionpool.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            if($this->input->post())
            {
            
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->question_pool_model->addNewPool($data);
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Question Pool created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Question Pool creation failed');
                }
                redirect('/setup/questionPool/list');
            }

            $this->global['pageTitle'] = 'Examination Management System : Add Question Pool';
            $this->global['pageCode'] = 'questionpool.add';
            $this->loadViews("questionpool/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('questionpool.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/setup/questionpool/list');
            }
            if($this->input->post())
            {
                $code = $this->security->xss_clean($this->input->post('code'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'code' => $code,
                    'name' => $name,
                    'status' => $status
                );
                
                $result = $this->question_pool_model->editPool($data,$id);
                if($result)
                {
                    $this->session->set_flashdata('success', 'Question Pool edited successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Question Pool edit failed');
                }
                redirect('/setup/questionPool/list');
            }

            $data['questionpool'] = $this->question_pool_model->getPool($id);

            $this->global['pageTitle'] = 'Examination Management System : Edit Question Pool';
            $this->global['pageCode'] = 'questionpool.edit';
            $this->loadViews("questionpool/edit", $this->global, $data, NULL);
        }
    }
}
