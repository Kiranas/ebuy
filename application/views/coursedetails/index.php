 
<section class="py-5">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 mr-auto">
        <div class="border border-light">
          <img class="w-100" src="<?php echo BASE_PATH;?>assets/images/<?php echo $courseDetails->file;?>" alt="">
        </div>
      </div>
      <div class="col-lg-6 mt-4">
        <h2><?php echo $courseDetails->name;?></h2>
        <ul class="list-inline">
          <li class="list-inline-item pr-3 border-right">
            <ul class="list-unstyled ec-review-rating font-size-12">
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
              <li class="active"><i class="fas fa-star"></i></li>
            </ul>
          </li>
          <li class="list-inline-item">3 customer reviews</li>
        </ul>
        <h4 class="mb-3">
          <span class="text-primary">Rs <?php echo $courseDetails->price;?></span>
          <span class="text-gray"><s>Rs <?php echo $courseDetails->price + 10;?></s></span>
        </h4>
        <p><i class="fas fa-check-circle text-success mr-2"></i>Available on stock</p>
        <div class="d-md-flex"> Quantity : 
          <select type='select' name='qty' id='qty' class="form-control" style="width: 76px;
    padding: 0.125rem 0.95rem;">
             <option value="1">1</option>
             <option value="2">2</option>
             <option value="3">3</option>
             <option value="4">4</option>
             <option value="5">5</option>
             <option value="6">6</option>
             <option value="7">7</option>
             <option value="8">8</option>
             <option value="9">9</option>
             <option value="10">10</option>
          </select>

        </div>
        
        <div class="d-md-flex">
          <button class="btn btn-primary mb-2 mr-3" onclick='buynow(<?php echo $courseDetails->id;?>,<?php echo $courseDetails->price;?>)'>Add to Cart</button>
        </div>
      </div>
    </div> <!-- END row-->

     <div class="row">
      <div class="col-lg-12 mt-4">
        <div class="mb-2">
          <h4>About the product</h4>
          <ul class="list-unstyled list-style-icon list-icon-bullet mt-3">
           <?php echo $courseDetails->message;?>
          </ul>
        </div>       
      </div>
    </div> <!-- END row-->


  </div> <!-- END container-->
</section>







 
<footer class="site-footer" style="color:white;">
  <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-100">
    <div class="container"> 
      <div class="row">

        <div class="col-lg-3 col-md-6 mt-5">
         <img src="assets/images/ezzibuy_logo_white.svg" alt="Logo" style="height: 150px;">
         <div class="margin-y-40">
           <p>
            Store of Homemade Happiness!!!!!
          </p>
         </div>
          <ul class="list-inline"> 
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-facebook"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-linkedin"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-pinterest"></i></a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 mt-5">
          <h4 class="h5 text-white">Contact Us</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-3"><i class="ti-headphone mr-3"></i><a href="tel:+8801740411513">93535 04127 </a></li>
            <li class="mb-3"><i class="ti-email mr-3"></i><a href="mailto:support@educati.com">contactus@ezzibuy.in</a></li>
            <li class="mb-3">
             <div class="media">
              <i class="ti-location-pin mt-2 mr-3"></i>
              <div class="media-body">
                <span> #451/A, 1st Cross, 2nd Main Road,Yelahanka, Bangalore- 560064</span>
              </div>
             </div>
            </li>
          </ul>
        </div>

        <div class="col-lg-2 col-md-6 mt-5">
          <h4 class="h5 text-white">Quick links</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-2"><a href="#">About Us</a></li>
            <li class="mb-2"><a href="#">Privacy Policy</a></li>
            <li class="mb-2"><a href="#">Terms and Condition</a></li>
          </ul>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <h4 class="h5 text-white">Newslatter</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <div class="marginTop-40">
            <p class="mb-4">Subscribe to get update and information. Don't worry, we won't send spam!</p>
            <form class="marginTop-30" action="#" method="POST">
              <div class="input-group">
                <input type="text" placeholder="Enter your email" class="form-control py-3 border-white" required="">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="submit">Subscribe</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div> <!-- END row-->
    </div> <!-- END container-->
  </div> <!-- END footer-top-->


</footer> <!-- END site-footer -->

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>
   function buynow(id,amount)
    {
      var quantity = $("#qty").val();
        $.get("/coursedetails/tempbuynow/"+id+"/"+amount+"/"+quantity, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>