<style>
  .error {
    display: grid;
  }
</style>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit TOS</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_unit" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Table of Specifications</h4>
      </div>

      <div class="form-container">


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $tos->name; ?>">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool[]" id="id_pool" class="form-control" multiple disabled>
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php if (in_array($record->id, explode(',', $tos->id_pool))) {
                                                                    echo "selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Number of Questions<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="question_count" name="question_count" value="<?php echo $tos->question_count; ?>" readonly>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if ($tos->status == 1) {
                                                                                                                      echo "checked=checked";
                                                                                                                    } ?>>
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if ($tos->status == 0) {
                                                                                                                      echo "checked=checked";
                                                                                                                    } ?>>
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
      <div>
        <h4 class="form-title">Question Details</h4>
      </div>
      <div class="form-container">


        <div class="col-12">
          <div class="custom-table">
            <table class="table">
              <thead>
                <tr>
                  <th>Sl. No</th>
                  <th>Course</th>
                  <th>Topic</th>
                  <th>Taxonomy</th>
                  <th>Difficulty</th>
                  <th>Available Questions</th>
                  <th>Choose</th>
                </tr>
              </thead>
              <tbody id="QuestionDetailsBody">
                <?php $i=1; if ($tosdetails) { 
                  foreach($tosdetails as $record){
                  ?>
                    <tr>
                      <td><?php echo $i; ?></td>
                      <td><?php echo $record->courseName; ?></td>
                      <td><?php echo $record->topicName; ?></td>
                      <td><?php echo $record->taxonomyName; ?></td>
                      <td><?php echo $record->levelName; ?></td>
                      <td><?php echo $record->questions_available; ?></td>
                      <td><input type='number' class='selectedQuestions' name='selectedQuestions[]' min='0' max='<?php echo $record->questions_available; ?>' value='<?php echo $record->questions_selected; ?>'></td>
                    </tr>
                <?php } } else { ?>
                  <tr>
                    <td colspan="7" class="text-center">No Data found.</td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>

        </div>
        <div class="button-block clearfix" >
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
          </div>
        </div>
      </div>
    </div>
  </form>
</main>

<script>
  $(document).ready(function() {
   
    $("#form_unit").validate({
      rules: {
        name: {
          required: true
        },
        id_pool: {
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Name Required</p>",
        },
        id_pool: {
          required: "<p class='error-text'>Pool Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
    $(document).on('blur', '#QuestionDetailsBody .selectedQuestions', function() {
      var count = 0;
      $('.selectedQuestions').each(function() {
        if ($(this).val()) {
          count += parseInt($(this).val());
        }
      });
      $('#question_count').val(count);
    });

  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>