<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Currency Setup</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

     <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl No</th>
            <th>Category Name</th>
            <th>Course</th>
            <th>Quantity</th>
            <th>Amount</th>
            <th>Total Amount</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($currencyList)) {
            $i = 1;
            foreach ($currencyList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->categoryname ?></td>
                <td><?php echo $record->coursename ?></td>
                <td><?php echo $record->price ?></td>
                <td><?php echo $record->quantity ?></td>
                <td><?php echo $record->amount ?></td>
                
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    
    <form id="form_unit" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Update Invoice Details</h4>
          </div>

            <div class="form-container">


                 <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Dispatched Date</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="dispact_date" name="dispact_date" placeholder=" Dispatched Date">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Reference Number <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="reference_number" name="reference_number" placeholder="Reference Number">
                        </div>
                      </div>
                    </div>

                    

                </div>
                
                  


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Completed</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

