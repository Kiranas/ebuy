<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Currency Setup List</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Currency</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Code / Name</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
                    </div>
                  </div>
                </div>

              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Amount</th>
            <th>Customer Name</th>
            <th>Phone</th>
            <th style="text-align: center;">Email</th>
            <th style="text-align: left;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($currencyList)) {
            $i = 1;
            foreach ($currencyList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->invoice_number ?></td>
                <td><?php echo $record->date_time ?></td>
                <td><?php echo $record->invoice_total ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->email ?></td>
                <td><?php echo $record->mobile ?></td>
                <td><a href="edit/<?php echo $record->id;?>">Details</a></td>
                
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }

  function makeDefault(id)
  {
     $.ajax(
        {
           url: '/finance/currency/makeDefault/'+id,
           type: 'GET',
           error: function()
           {
            alert('Something is wrong');
           },
           success: function(result)
           {
            window.location.reload();
           }
        });
  }
</script>