<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Topic_model extends CI_Model
{

    function topicListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function topicListSearch($data)
    {
        $this->db->select('c.*,course.name as courseName');
        $this->db->from('topic as c');
        $this->db->join('course', 'course.id = c.id_course');

        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if (!empty($data['id_course']))
        {
            $this->db->where('c.id_course', $data['id_course']);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getTopic($id)
    {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewTopic($data)
    {
        $this->db->trans_start();
        $this->db->insert('topic', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editTopic($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('topic', $data);

        return $result;
    }

    function deleteTopic($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('topic', $data);
        return $this->db->affected_rows();
    }
}
