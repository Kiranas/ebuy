 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
             
              
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('course.list','course.add','course.edit','topic.list','topic.add','topic.edit','learningobjective.list','learningobjective.add','learningobjective.edit','taxonomy.list','taxonomy.add','taxonomy.edit','difficultylevel.list','difficultylevel.add','difficultylevel.edit','question.list','question.add','question.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>Functional Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('course.list','course.add','course.edit','category.list','category.add','category.edit','category.add_module','unit.list','unit.add','unit.edit','unit.marks_distribution'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                  
                  <li class="nav-item">
                    <a href="/setup/category/list" class="nav-link <?php if(in_array($pageCode,array('category.list','category.add','category.edit','category.add_module'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Category</span>                      
                    </a>
                  </li>
                  
                  <li class="nav-item">
                    <a href="/setup/course/list" class="nav-link <?php if(in_array($pageCode,array('course.list','course.add','course.edit'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Product</span>                      
                    </a>
                  </li>

               

                </ul>
              </li>                
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>