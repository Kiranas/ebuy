<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_session_model extends CI_Model
{
    function getCentersByLocatioin($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_session as a');
        $this->db->join('exam_session_location as ecl', 'a.id_location = ecl.id');
            $this->db->where('a.id_location', $id_location);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

    function examSessionListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, ecl.name as exam_set, ec.name as exam_center');
        $this->db->from('exam_session as a');
        $this->db->join('exam_set as ecl', 'a.id_exam_set = ecl.id');
        $this->db->join('exam_center as ec', 'a.id_exam_center = ec.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_exam_set'] != '')
        {
            $this->db->where('a.id_exam_set', $data['id_exam_set']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('a.id_exam_center', $data['id_exam_center']);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;

         // $list = array();
         // foreach ($result as $value)
         // {
         //    $data = $this->getExamCenterNLocationByCenterId($value->id_exam_session);
         //    $value->exam_session = $data['exam_session'];
         //    $value->location = $data['location'];
            
         //    array_push($list, $value);
         // }
    }

    function getExamSession($id)
    {
        $this->db->select('*');
        $this->db->from('exam_session');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamSession($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_session', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamSession($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_session', $data);
        return $result;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examSetListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_session_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterList()
    {
        $this->db->select('*');
        $this->db->from('exam_session');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_session');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_session'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_session_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }
}