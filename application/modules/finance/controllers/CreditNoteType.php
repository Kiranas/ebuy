<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class CreditNoteType extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('credit_note_type_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('credit_note_type.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $data['searchParam'] = $formData;
            $data['creditNoteTypeList'] = $this->credit_note_type_model->creditNoteTypeListSearch($formData);

            $this->global['pageTitle'] = 'Campus Management System : Credit Note Type List';
            $this->global['pageCode'] = 'credit_note_type.list';

            $this->loadViews("credit_note_type/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('credit_note_type.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                //echo "<Pre>"; print_r($subjectDetails);exit;

                $result = $this->credit_note_type_model->addNewCreditNoteType($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'Credit Note Type added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Credit Note Type add failed');
                }
                redirect('/finance/creditNoteType/list');
            }
            $this->global['pageTitle'] = 'Campus Management System : Add CreditNoteType';
            $this->global['pageCode'] = 'credit_note_type.add';

            $this->loadViews("credit_note_type/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('credit_note_type.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/creditNoteType/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->credit_note_type_model->editCreditNoteType($data,$id);
                redirect('/finance/creditNoteType/list');
            }
            $data['creditNoteType'] = $this->credit_note_type_model->getCreditNoteType($id);
            $this->global['pageTitle'] = 'Campus Management System : Edit CreditNoteType';
            $this->global['pageCode'] = 'credit_note_type.edit';

            $this->loadViews("credit_note_type/edit", $this->global, $data, NULL);
        }
    }
}
