<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Currency_model extends CI_Model
{
    function getPendingInvoice()
    {
        $this->db->select('inv.*,c.name,c.email,c.mobile');
        $this->db->from('main_invoice as inv');
        $this->db->join('customer as c', 'c.id = inv.id_customer');
        $this->db->where("inv.completed_order='0'");

        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

      function getCompletedInvoice()
    {
        $this->db->select('inv.*,c.name,c.email,c.mobile');
        $this->db->from('main_invoice as inv');
        $this->db->join('customer as c', 'c.id = inv.id_customer');
        $this->db->where("inv.completed_order='1'");

        $this->db->order_by("id", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

      function getinvoiceDetails($id)
     { $this->db->select('mid.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('product as c', 'mid.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('mid.id_main_invoice', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }




 function updateInvoice($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);
        return TRUE;
    }


   
}

