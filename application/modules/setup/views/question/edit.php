<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit Question</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Question Details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool" id="id_pool" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_pool) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Course <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course" id="id_course" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseList)) {
                    foreach ($courseList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_course) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>


        </div>


        <div class="row">


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Topic <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_topic" id="id_topic" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($topicList)) {
                    foreach ($topicList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_topic) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>



          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Course Learning Objective <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseLearningObjectList)) {
                    foreach ($courseLearningObjectList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_course_learning_objective) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Bloom Taxonomy <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($bloomTaxonomyList)) {
                    foreach ($bloomTaxonomyList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_bloom_taxonomy) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Difficult Level <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($difficultLevelList)) {
                    foreach ($difficultLevelList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_difficult_level) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

        </div>


        <div class="row">


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Upload Image

                <?php
                if (!empty($question->image)) {
                ?>
                  <span class='error-text'>*</span>

                <?php

                }
                ?>


              </label>
              <?php
              if ($question->image) {
              ?>
                <a href="<?php echo '/assets/images/' . $question->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $question->image; ?>)" title="<?php echo $question->image; ?>">
                  View
                </a>
              <?php

              }
              ?>


              </label>
              <div class="col-sm-8">
                <input type="file" class="form-control" id="image_file" name="image_file">
              </div>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" <?php if ($question->status == 1) {
                                                                                                                      echo "checked=checked";
                                                                                                                    }; ?>>
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0" <?php if ($question->status == 0) {
                                                                                                                      echo "checked=checked";
                                                                                                                    }; ?>>
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="question" id="question"><?php echo $question->question; ?></textarea>
              </div> 
            </div>
          </div>
        </div>


        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
            <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script>
  $(document).ready(function() {
    CKEDITOR.replace( 'question' );
    $("#form_main").validate({
      rules: {
        id_pool: {
          required: true
        },
        id_course: {
          required: true
        },
        id_topic: {
          required: true
        },
        id_course_learning_objective: {
          required: true
        },
        id_bloom_taxonomy: {
          required: true
        },
        id_difficult_level: {
          required: true
        }
      },
      messages: {
        description: {
          id_pool: "<p class='error-text'>Question Pool</p>",
        },
        id_course: {
          required: "<p class='error-text'>Select Course</p>",
        },
        id_topic: {
          required: "<p class='error-text'>Select Topic</p>",
        },
        id_course_learning_objective: {
          required: "<p class='error-text'>Select Course Learining Object</p>",
        },
        id_bloom_taxonomy: {
          required: "<p class='error-text'>Select Bloom Taxonomy</p>",
        },
        id_difficult_level: {
          required: "<p class='error-text'>Select Difficult Level</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#id_course').change(function() {
      var id = $(this).val();
      $.ajax({
        url: "<?php echo site_url('setup/ajax/getCategoryTopics'); ?>",
        method: "POST",
        data: {
          id: id
        },
        async: true,
        dataType: 'json',
        success: function(data) {

          var html = '';
          var i;
          for (i = 0; i < data.length; i++) {
            html += '<option value=' + data[i].id + '>' + data[i].name + '-' + data[i].name + '</option>';
          }
          $('#id_topic').html(html);

        }
      });
      return false;
    });

  });
</script>