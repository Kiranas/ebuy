<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Topic</h1>

    <a href='list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Topic details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Code <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="code" name="code" placeholder="Code">
              </div>
            </div>
          </div>


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" placeholder="Name">
              </div>
            </div>
          </div>

        </div>



        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Course <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course" id="id_course" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseList)) {
                    foreach ($courseList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>  
          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                  <label class="custom-control-label" for="customRadioInline1">Active</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                  <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                </div>
              </div>
            </div>
          </div>

        </div>



        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
            <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>

<script>
  $(document).ready(function() {
    $("#form_main").validate({
      rules: {
        code: {
          required: true
        },
        name: {
          required: true
        },
        id_course: {
          required: true
        }
      },
      messages: {
        code: {
          required: "<p class='error-text'>Code Required</p>",
        },
        name: {
          required: "<p class='error-text'>Name Required</p>",
        },
        id_course: {
          required: "<p class='error-text'>Course Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
  function reloadPage() {
    window.location.reload();
  }
</script>