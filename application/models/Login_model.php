<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Login_model extends CI_Model
{
    
    /**
     * This function used to check the login credentials of the user
     * @param string $email : This is email of the user
     * @param string $password : This is encrypted password of the user
     */
    function loginMe($email, $password)
    {
        $this->db->select('usr.*');
        $this->db->from('customer as usr');
        $this->db->where('usr.email', $email);
        $this->db->where('usr.password', $password);
        $query = $this->db->get();
        
        $user = $query->row();
        return $user;
    }

    function loginAdmin($email, $password)
    {
        $this->db->select('usr.*, usr.id as userId, Roles.role');
        $this->db->from('users as usr');
        $this->db->join('roles as Roles','usr.id_role = Roles.id','left');
        $this->db->where('usr.email', $email);
        $this->db->where('usr.password', $password);
        $query = $this->db->get();
        
        $user = $query->row();

        return $user;
    }

    function lastLoginInfo($userId)
    {
        $this->db->select('usr.created_dt_tm');
        $this->db->where('usr.id_user', $userId);
        $this->db->order_by('usr.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('user_last_login as usr');

        return $query->row();
    }

    function lastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('user_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}
?>