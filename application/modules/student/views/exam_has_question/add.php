<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Exam Has Question</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Has uestion details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">No. Of Questions To Add <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="question_count" name="question_count" placeholder="No. Questions">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_course" id="id_course" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>


                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Topic </label>
                          <div class="col-sm-8">
                            <select name="id_topic" id="id_topic" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Course Learning Objective </label>
                          <div class="col-sm-8">
                            <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                </div>


                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Bloom Taxonomy </label>
                          <div class="col-sm-8">
                            <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Difficult Level </label>
                          <div class="col-sm-8">
                            <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                </div>
                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                question_count: {
                    required: true
                },
                id_course: {
                    required: true
                }
            },
            messages: {
                question_count: {
                    required: "<p class='error-text'>No. Of Questions Required</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
          yearRange: "1960:2001"
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>