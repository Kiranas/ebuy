 

 
  
  
   
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
  
  <div class="carousel-inner" style="height: 350px;">
   
    <div class="carousel-item height-90vh padding-y-80 active">
     <div class="bg-absolute" data-dark-overlay="5" style="background:url(website/images/1.jpg) no-repeat"></div>
       
    </div>
   
    <div class="carousel-item height-90vh padding-y-80">
      <div class="bg-absolute" data-dark-overlay="5" style="background:url(website/images/slider2.png) no-repeat"></div>
       
    </div>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <i class="ti-angle-left iconbox bg-black-0_5 hover:primary"></i>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <i class="ti-angle-right iconbox bg-black-0_5 hover:primary"></i>
  </a>
</div>
    
    

<section class="padding-y-100 bg-light">
  <div class="container">
    <div class="row">
    <div class="col-12 text-center text-white mb-5">
        <h3 style='color:black;'>
          Our Trending Categories are 
        </h3>
        <div class="width-3rem height-4 rounded bg-primary mx-auto"></div>
      </div>
    </div>
    <div class="col-12">
       <div class="owl-carousel arrow-on-hover"
         data-space="30"
         data-arrow="true"
         >
         

         <?php for($i=0;$i<count($categoryList);$i++) { 

         ?> 
        <div class="card text-gray height-100p shadow-v1">
         <div class="position-relative">
           <a href="">
             <img class="card-img-top" style='height:200px;' src="assets/images/<?php echo $categoryList[$i]->image;?>" style="height:200px;" alt="">
           </a>
         </div>
          <div class="card-body">
            <a href="product/index/<?php echo $categoryList[$i]->url_name;?>" class="h6" style="text-transfors: uppercase;">
              <?php echo $categoryList[$i]->name;?>
            </a>
          </div>
        </div>

      <?php } ?>
         
      
       
       </div>
     </div>
  </div> <!-- END container-->
</section>



<section class="paddingTop-50 paddingBottom-100 bg-light-v2">
  <div class="container">
   
    <div class="row marginTop-50">
      <div class="col-md-6 my-2">
        <h3>
          Instant Food Products
        </h3>
      </div>
      <div class="col-md-6 my-2 text-md-right">
        <a href="/product/index/snacks" class="btn btn-outline-primary">All Foods</a>
      </div>
      <div class="col-12 mt-2">
        <div class="owl-carousel arrow-on-hover" 
        data-items="4"
        data-state-outer-class="py-4"
        data-arrow="true"
        data-autoplay="false"
        data-space="30"
        data-loop="true">


        <?php for($j=0;$j<count($firstCourseList);$j++) { ?>
         
          <div class="card text-gray height-100p shadow-v2">
            <a href="">
              <img class="card-img-top" style='height:200px;' src="assets/images/<?php echo $firstCourseList[$j]->file;?>" alt="">
            </a>
            <div class="p-4">
              <a href="<?php echo BASE_PATH;?>coursedetails/<?php echo $firstCourseList[$j]->id;?>" class="h6" style='height:59px;'>
                <?php echo $firstCourseList[$j]->name;?>
              </a>
            </div>
            <div class="media border-top p-4 align-items-center justify-content-between">
              <h4 class="mb-3">
                <span class="text-primary">Rs <?php echo $firstCourseList[$j]->price;?></span>
              </h4>
              
            </div>
             <div class="media border-top p-4 align-items-center justify-content-between">
              <button class="btn btn-primary mb-2 mr-3" onclick='buynow(<?php echo $firstCourseList[$j]->id;?>,<?php echo $firstCourseList[$j]->price;?>)'>Add to Cart</button>
              
            </div>
         </div>
          <?php } ?> 
         
         


        </div>
      </div>
    </div> <!-- END row-->
    

  </div> <!-- END container-->
</section>   <!-- END section-->
 
   

   
 
<footer class="site-footer" style="color:white;">
  <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-100">
    <div class="container"> 
      <div class="row">

        <div class="col-lg-3 col-md-6 mt-5">
         <img src="assets/images/ezzibuy_logo_white.svg" alt="Logo" style="height: 150px;">
         <div class="margin-y-40">
           <p>
            Store of Homemade Happiness!!!!!
          </p>
         </div>
          <ul class="list-inline"> 
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-facebook"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-linkedin"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-pinterest"></i></a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 mt-5">
          <h4 class="h5 text-white">Contact Us</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-3"><i class="ti-headphone mr-3"></i><a href="tel:+8801740411513">93535 04127 </a></li>
            <li class="mb-3"><i class="ti-email mr-3"></i><a href="mailto:support@educati.com">contactus@ezzibuy.in</a></li>
            <li class="mb-3">
             <div class="media">
              <i class="ti-location-pin mt-2 mr-3"></i>
              <div class="media-body">
                <span> #451/A, 1st Cross, 2nd Main Road,Yelahanka, Bangalore- 560064</span>
              </div>
             </div>
            </li>
          </ul>
        </div>

        <div class="col-lg-2 col-md-6 mt-5">
          <h4 class="h5 text-white">Quick links</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-2"><a href="/index/aboutus">About Us</a></li>
            <li class="mb-2"><a href="/index/privacy">Privacy Policy</a></li>
            <li class="mb-2"><a href="/index/terms">Terms and Condition</a></li>
          </ul>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <h4 class="h5 text-white">Newslatter</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <div class="marginTop-40">
            <p class="mb-4">Subscribe to get update and information. Don't worry, we won't send spam!</p>
            <form class="marginTop-30" action="#" method="POST">
              <div class="input-group">
                <input type="text" placeholder="Enter your email" class="form-control py-3 border-white" required="">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="submit">Subscribe</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div> <!-- END row-->
    </div> <!-- END container-->
  </div> <!-- END footer-top-->


</footer> <!-- END site-footer -->

<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="website/js/vendors.bundle.js"></script>
    <script src="website/js/scripts.js"></script>
  </body>
</html>


<script>
   function buynow(id,amount)
    {
      var quantity = 1;
        $.get("/coursedetails/tempbuynow/"+id+"/"+amount+"/"+quantity, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

  </script>