<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';
require(BASEPATH.'libraries/config.php');
require(BASEPATH.'libraries/razorpay-php/Razorpay.php');

// Create the Razorpay Order

use Razorpay\Api\Api;

class Dashboard extends BaseController
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('dashboard_model');
        $this->customerId =  $this->session->userdata['id'];
        error_reporting(0);
    }

    function index()
    {


        $id_session = session_id();


        if(!$this->customerId)
        {
            redirect('/index');
        }


        if($this->input->post())
             {

                      $billing['name'] = $this->input->post('name');
            $billing['email'] = $this->input->post('email');
            $billing['mobile'] = $this->input->post('mobile');

            $this->dashboard_model->updateProfile($billing,$this->customerId);

             }

       
        $data['customerDetails'] = $this->dashboard_model->getCustomerDetails($this->customerId);

      
            
        $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
        $this->loadViews("dashboard/index", $this->global, $data, NULL);
    }

    function course()
    {

        $data['courseList'] = $this->dashboard_model->getCoursesByStudent($this->customerId);
            $this->loadViews("dashboard/course", $this->global, $data, NULL);
    }

    function soa()
    {

           $data['invoiceList'] = $this->dashboard_model->getInvoices($this->customerId);
        
           $data['receiptList'] = $this->dashboard_model->getReceipt($this->customerId);

            $this->global['pageTitle'] = 'Examination Management System : Exam Has Question List';
            $this->loadViews("dashboard/soa", $this->global, $data, NULL);
    }

     function checkout() {

       $id_session = session_id();

        $data['listOfCourses'] = $this->dashboard_model->getCourses($id_session);
        
        $this->loadViews('dashboard/checkout',$this->global,$data,NULL);

    }


    

    public function payment() {


         $id_session = session_id();
        $listOfCourses = $this->dashboard_model->getCourses($id_session);
$total_amount = 0;

        for($k=0;$k<count($listOfCourses);$k++) {
           


             $detail_data = array(
                                'id_main_invoice' => $id_invoice,
                                'id_product' => $listOfCourses[$k]->id_product,
                                'amount' => $listOfCourses[$k]->amount * $listOfCourses[$k]->quantity,
                                'price' => $listOfCourses[$k]->amount,
                                'quantity' => $listOfCourses[$k]->quantity,
                                
                            );
                        
                            $total_amount = $total_amount + $listOfCourses[$k]->amount * $listOfCourses[$k]->quantity;

        }


                    $billingInfo = $this->dashboard_model->getBillingDetailsById($this->session->userdata['billing_address']);


                

// print_r($billingInfo);exit;
        require(BASEPATH.'libraries/config.php');
        require(BASEPATH.'libraries/razorpay-php/Razorpay.php');
        $api = new Api($keyId, $keySecret);

        //
        // We create an razorpay order using orders api
        // Docs: https://docs.razorpay.com/docs/orders
        
        $orderData = [
            'amount'          => $total_amount * 100, // 2000 rupees in paise
            'currency'        => 'INR',
            'payment_capture' => 1 // auto capture
        ];

        $razorpayOrder = $api->order->create($orderData);

        $razorpayOrderId = $razorpayOrder['id'];

        $_SESSION['razorpay_order_id'] = $razorpayOrderId;

        $displayAmount = $amount = $orderData['amount'];

        if ($displayCurrency !== 'INR')
        {
            $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
            $exchange = json_decode(file_get_contents($url), true);

            $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
        }

        $checkout = 'automatic';

        if (isset($_GET['checkout']) and in_array($_GET['checkout'], ['automatic', 'manual'], true))
        {
            $checkout = $_GET['checkout'];
        }

        $cartdata = [
            "key"               => $keyId,
            "amount"            => $amount,
            "name"              => $billingInfo->name,
            "description"       => "Ezzibuy Billing Details",
            "prefill"           => [
            "name"              => $billingInfo->name,
            "email"             => $billingInfo->email,
            "contact"           => $billingInfo->phone,
            ],
           
            "theme"             => [
            "color"             => "#F37254"
            ],
            "order_id"          => $razorpayOrderId,
        ];

        if ($displayCurrency !== 'INR')
        {
            $cartdata['display_currency']  = $displayCurrency;
            $cartdata['display_amount']    = $displayAmount;
        }



        $data['checkoutdata'] = $cartdata;
        $data['listOfCourses'] = $listOfCourses;
        $this->loadViews('dashboard/payment',$this->global,$data,NULL);

    }

    public function billing() {

       $id_session = session_id();

       
     

        if($this->input->post())
             {

            $billing['name'] = $this->input->post('billing-name');
            $billing['address'] = $this->input->post('billing-address');
            $billing['address_two'] = $this->input->post('billing-address2');
            $billing['city'] = $this->input->post('billing-city');
            $billing['state'] = $this->input->post('billing-state');
            $billing['zip'] = $this->input->post('billing-zip');
            $billing['email'] = $this->input->post('billing-email');
            $billing['phone'] = $this->input->post('billing-phone');
            $billing['id_customer'] = $this->customerId;


      
            $this->session->userdata['billing_address'] = $this->dashboard_model->insertBilling($billing);


            $shipping['name'] = $this->input->post('shipping-name');
            $shipping['address'] = $this->input->post('shipping-address');
            $shipping['address_two'] = $this->input->post('shipping-address2');
            $shipping['city'] = $this->input->post('shipping-city');
            $shipping['state'] = $this->input->post('shipping-state');
            $shipping['zip'] = $this->input->post('shipping-zip');
            $shipping['email'] = $this->input->post('shipping-email');
            $shipping['phone'] = $this->input->post('shipping-phone');
            $shipping['id_customer'] = $this->customerId;

            $this->session->userdata['shipping_address'] = $this->dashboard_model->insertShipping($shipping);


                    redirect('/profile/dashboard/payment');


        }

         $data['listOfCourses'] = $this->dashboard_model->getCourses($id_session);

         

         //check for billing previous address and shipping previous address
        $data['billingaddress'] = $this->dashboard_model->getpreviousBilling($this->customerId);

        if(empty($data['billingaddress'])) {


        $data['billingaddress'] = $this->dashboard_model->getCustomerDetails($this->customerId);


$data['billingaddress']->phone = $data['billingaddress']->mobile;


          
        }
        $data['shippingaddress'] = $this->dashboard_model->getpreviousShipping($this->customerId);





        $this->loadViews('dashboard/billing',$this->global,$data,NULL);

    }

    public function success(){



        if($_POST['razorpay_payment_id']) {
            $id_session = session_id();
            $listOfCourses = $this->dashboard_model->getCourses($id_session);
            $invoice_number = $this->dashboard_model->generateMainInvoiceNumber();
            $invoice_data = array(
                'id_customer' => $this->customerId,
                'status' => 1,
                'invoice_number' => $invoice_number,
                'id_billing_address' => $this->session->userdata['billing_address'],
                                    'id_shipping_address' => $this->session->userdata['shipping_address']
            );

            $id_invoice = $this->dashboard_model->addInvoice($invoice_data);

            for($k=0;$k<count($listOfCourses);$k++) {
               


                 $detail_data = array(
                                    'id_main_invoice' => $id_invoice,
                                    'id_product' => $listOfCourses[$k]->id_product,
                                    'amount' => $listOfCourses[$k]->amount * $listOfCourses[$k]->quantity,
                                    'price' => $listOfCourses[$k]->amount,
                                    'quantity' => $listOfCourses[$k]->quantity,
                                    
                                );
                            
                                $id_invoice_details = $this->dashboard_model->addInvoiceDetails($detail_data);
                                $total_amount = $total_amount + $listOfCourses[$k]->amount * $listOfCourses[$k]->quantity;


            }

                            //$update_invoice['total_amount'] = $total_amount;
            $update_invoice['invoice_total'] = $total_amount;
                            //$update_invoice['balance_amount'] = $total_amount;

            $id_updated_invoice = $this->dashboard_model->editInvoice($update_invoice,$id_invoice);

            $receipt['invoice_id'] = $id_invoice;
               $this->generateReceipt($receipt);
     $student_list_data = $this->dashboard_model->deletefromtempBySessionid($id_session);

            $this->global = '';
            $data = '';
            $this->loadViews('dashboard/success',$this->global,$data,NULL);
    }


    }

    function deletetemp($id) {
         $student_list_data = $this->dashboard_model->deletefromtemp($id);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }


    function getStateByCountry($id_country)
    {
        $results = $this->dashboard_model->getStateByCountryId($id_country);

        // echo "<Pre>"; print_r($programme_data);exit;
        $table="
            <script type='text/javascript'>
                 $('select').select2();
            </script>";

        $table.="
        <select name='state' id='state' class='custom-select d-block w-100' required=''>
            <option value=''>Choose...</option>
            ";

        for($i=0;$i<count($results);$i++)
        {
            // $id = $results[$i]->id_procurement_category;
            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";
        }

        $table.="

        </select>";

        echo $table;
        exit;
    }

    function invoice()
    {
        $data = $this->security->xss_clean($this->input->post('tempData'));
        $this->generateInvoice($data);
    }


     function invoicedetails($id)
    {
               $id_session = session_id();

        $data['listOfCourses'] = $this->dashboard_model->getinvoiceDetails($id);
        $this->global = '';
        $this->loadViews('dashboard/invoicedetails',$this->global,$data,NULL);

    }


    function generateInvoice($data)
    {
         echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Invoice';
        }
        else
        {
            $id_category = $data['id_category'];
            $id_course = $data['id_product'];
            $id_student = $data['id_student'];

            $fee_structure = $this->dashboard_model->getFeestructureByData($data);

            $course = $this->dashboard_model->getCourseByData($data);

            if(empty($course))
            {
                echo 'No Module Found';exit;
            }

            if($fee_structure)
            {
                $fee_structure_details = $this->dashboard_model->getFeestructureDetailsByIdFeeStructureMain($fee_structure->id);

                if($fee_structure_details)
                {
                    $invoice_number = $this->dashboard_model->generateMainInvoiceNumber();

                    $invoice_data = array(
                        'id_category' => $id_category,
                        'id_product' => $id_course,
                        'id_user' => $id_student,
                        'status' => 1,
                        'invoice_number' => $invoice_number
                    );

                    $id_invoice = $this->dashboard_model->addInvoice($invoice_data);

                    if($id_invoice)
                    {
                        $total_amount = 0;

                        foreach ($fee_structure_details as $detail)
                        {
                            $detail_data = array(
                                'id_main_invoice' => $id_invoice,
                                'id_fee_item' => $detail->id_fee_item,
                                'amount' => $detail->amount,
                                'price' => $detail->amount,
                                'quantity' => 1,
                            );
                        
                            $id_invoice_details = $this->dashboard_model->addInvoiceDetails($detail_data);
                            $total_amount = $total_amount + $detail->amount;
                        }

                        $months = "+". $course->months ." months";
                        $expiry_date = date("Y-m-d", strtotime($months));

                        // $expiry_date = date(("Y-m-d") . $months);


                        $data['id_invoice'] = $id_invoice;
                        $data['status'] = 1;
                        $data['amount'] = $total_amount;
                        $data['expiry_date'] = $expiry_date;

                        // echo "<Pre>"; print_r($id_invoice);exit();

                        $added_student_has_course = $this->dashboard_model->addStudentHasCourse($data);

                        $update_invoice['total_amount'] = $total_amount;
                        $update_invoice['invoice_total'] = $total_amount;
                        $update_invoice['balance_amount'] = $total_amount;

                        $id_updated_invoice = $this->dashboard_model->editInvoice($update_invoice,$id_invoice);
                    
                        // echo "<Pre>"; print_r($id_updated_invoice);exit();

                        return $id_invoice ;

                    }

                }
                else
                {
                    echo 'No Fee Structure Is Defined For Entered Data';exit;
                }


            }
            else
            {
                echo 'No Fee Structure Is Defined For Entered Data';exit;
            }


        }
    }


    function generateReceipt($data)
    {
        // echo "<Pre>";print_r($data);exit;
        if($data == '')
        {
            echo 'No Parameters Passed To Generate the Receipt';
        }
        else
        {
            $id_invoice = $data['invoice_id'];

            $main_invoice = $this->dashboard_model->getInvoice($id_invoice);

            if($main_invoice)
            {
                    $receipt_number = $this->dashboard_model->generateReceiptNumber();

                    $invoice_data = array(
                        'id_customer' => $main_invoice->id_customer,
                        'status' => 1,
                        'receipt_number' => $receipt_number,
                        'receipt_amount' => $main_invoice->invoice_total
                    );

                    $id_receipt = $this->dashboard_model->addReceipt($invoice_data);

                    if($id_receipt)
                    {
                        $total_amount = 0;

                        $detail_data = array(
                            'id_receipt' => $id_receipt,
                            'id_main_invoice' => $id_invoice,
                            'invoice_amount' => $main_invoice->invoice_total,
                            'paid_amount' => $main_invoice->invoice_total,
                            'status' => 1,
                        );
                    
                        $id_invoice_details = $this->dashboard_model->addReceiptDetails($detail_data);
                        

                        echo "Receipt Generated : " . $receipt_number;
                    }
                

            }
            else
            {
                echo 'No Main Invoice Found For The Entered Invoice';
            }

            return 1;


        }

    }
}