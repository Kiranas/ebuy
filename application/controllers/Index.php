<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Index extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        $this->load->model('register_model');

        parent::__construct();
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {

$data['categoryList'] = $this->register_model->getCategory();

$data['firstCourseList'] = $this->register_model->getAllCourses(1);
$data['secondCourseList'] = $this->register_model->getAllCourses(2);
$data['thirdCourseList'] = $this->register_model->getAllCourses(3);


                $this->loadViews('index/index',$this->global,$data,NULL);
        
    }


     public function login()
    {
                $this->load->view('index/login');

        
    }

     public function aboutus()
    {
        $data['aboutus'] = $this->register_model->aboutus();

                $this->loadViews('index/aboutus',$this->global,$data,NULL);

        
    }


      public function privacy()
    {
        $data['aboutus'] = $this->register_model->aboutus();

                $this->loadViews('index/privacy',$this->global,$data,NULL);

        
    }

      public function terms()
    {
        $data['aboutus'] = $this->register_model->aboutus();

                $this->loadViews('index/terms',$this->global,$data,NULL);

        
    }

    public function checkout() {

       $id_session = session_id();

        $data['listOfCourses'] = $this->register_model->getCourses($id_session);
        
        $this->loadViews('index/checkout',$this->global,$data,NULL);

    }


    function deletetemp($id) {
         $student_list_data = $this->register_model->deletefromtemp($id);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }

    function updateqty($id,$amount){
         $student_list_data = $this->register_model->updatetemp($id,$amount);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }

    


    public function logout()
    {

        $sessionArray = array('id'=> '',
                    'customerLoggedIn' => FALSE
            );
     $this->session->set_userdata($sessionArray);


                            redirect('index');


        
    }
    
}