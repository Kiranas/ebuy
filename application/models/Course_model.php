<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Course_model extends CI_Model
{
	 function addtotemp($data) {
	 	$result = $this->db->insert('temp_cart', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }

	 function checkid($data) {
	 	$this->db->select('tc.*');
        $this->db->from('temp_cart as tc ');
        
        $this->db->where('tc.id_session', $data['id_session']);
        $this->db->where('tc.id_product', $data['id_product']);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
	 }
}
