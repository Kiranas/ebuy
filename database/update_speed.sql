
CREATE TABLE `currency_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(20) DEFAULT 0,
  `default` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `currency_rate_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_currency` int(20) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;






CREATE TABLE `financial_account_code` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(2) DEFAULT '0',
  `id_parent` int(20) DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `financial_account_code` ADD `type` VARCHAR(512) NULL DEFAULT '' AFTER `name`;




CREATE TABLE `bank_registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `account_no` varchar(512) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(20) DEFAULT NULL,
  `id_country` int(20) DEFAULT NULL,
  `zipcode` int(10) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `payment_type` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');



CREATE TABLE `fee_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `fee_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(10) DEFAULT NULL,
  `id_amount_calculation_type` int(10) DEFAULT NULL,
  `id_frequency_mode` int(10) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(2) DEFAULT NULL,
  `is_non_invoice` int(2) DEFAULT NULL,
  `is_gst` int(2) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `category` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `category_has_module` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `course` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `id`;

ALTER TABLE `course` ADD `file` VARCHAR(512) NULL DEFAULT '' AFTER `name_optional_language`;


CREATE TABLE `fee_structure_main` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_currency` int(20) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `fee_structure_main` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `id_currency`;




CREATE TABLE `temp_fee_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_session` varchar(512) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;





CREATE TABLE `fee_structure` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_fee_structure` int(20) DEFAULT 0,
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `id_fee_item` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `applicant` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `passport` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `contact_email` varchar(500) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `id_type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT '0',
  `id_course` int(20) DEFAULT '0',
  `passport_expiry_date` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `date_of_birth` varchar(120) DEFAULT '',
  `martial_status` varchar(120) DEFAULT '',
  `religion` varchar(120) DEFAULT '',
  `nationality` varchar(120) DEFAULT '',
  `is_hostel` int(20) DEFAULT '0',
  `id_degree_type` int(20) DEFAULT '0',
  `id_race` varchar(120) DEFAULT '',
  `mail_address1` varchar(120) DEFAULT '',
  `mail_address2` varchar(120) DEFAULT '',
  `mailing_country` int(120) DEFAULT NULL,
  `mailing_state` int(120) DEFAULT NULL,
  `mailing_city` varchar(120) DEFAULT '',
  `mailing_zipcode` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(120) DEFAULT NULL,
  `permanent_state` int(120) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `sibbling_discount` varchar(50) DEFAULT '',
  `employee_discount` varchar(50) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `passport_number` varchar(50) DEFAULT '',
  `is_updated` int(2) DEFAULT '0',
  `is_submitted` int(2) DEFAULT '0',
  `submitted_date` datetime DEFAULT NULL,
  `email_verified` int(11) DEFAULT '0',
  `initial` int(2) DEFAULT '0',
  `status` int(20) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `applicant_last_login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_applicant` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `race_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `race_setup`
--

INSERT INTO `race_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malays', '', 1, NULL, '2020-07-10 23:24:07', NULL, '2020-07-10 23:24:07'),
(2, 'Chinese', '', 1, NULL, '2020-07-10 23:24:14', NULL, '2020-07-10 23:24:14'),
(3, 'Indian', '03', 1, NULL, '2020-08-27 17:14:35', NULL, '2020-08-27 17:14:35'),
(4, 'jawa', '04', 1, NULL, '2020-08-27 17:15:24', NULL, '2020-08-27 17:15:24');


CREATE TABLE `religion_setup` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `religion_setup`
--

INSERT INTO `religion_setup` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Islam', 'ISLAM', 1, NULL, '2020-07-10 23:24:44', NULL, '2020-07-10 23:24:44'),
(2, 'Buddhism', 'Buddhism', 1, NULL, '2020-07-10 23:24:56', NULL, '2020-07-10 23:24:56');


ALTER TABLE `student` ADD `id_category` INT(20) NULL DEFAULT '0' AFTER `passport_number`, ADD `id_course` INT(20) NULL DEFAULT '0' AFTER `id_category`;

ALTER TABLE `applicant` ADD `approved_dt_tm` DATETIME NULL DEFAULT NULL AFTER `approved_by`;


DROP TABLE student;


CREATE TABLE `student` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `first_name` varchar(500) DEFAULT NULL,
  `last_name` varchar(500) DEFAULT NULL,
  `nric` varchar(500) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `password` varchar(500) DEFAULT NULL,
  `mail_address1` varchar(500) DEFAULT NULL,
  `mail_address2` varchar(500) DEFAULT NULL,
  `mailing_country` int(20) DEFAULT NULL,
  `mailing_state` int(20) DEFAULT NULL,
  `mailing_zipcode` varchar(500) DEFAULT NULL,
  `mailing_city` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `unit` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(1024) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `id_category` int(20) NULL DEFAULT 0,
  `id_course` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `marks_distribution` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_unit` bigint(20) NULL DEFAULT 0,
  `module` varchar(1024) DEFAULT '',
  `max_marks` int(20) NULL DEFAULT 0,
  `pass_marks` int(20) NULL DEFAULT 0,
  `is_pass_compulsary` int(20) NULL DEFAULT 0,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `main_invoice` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_course` int(20) DEFAULT 0,
  `invoice_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_application` int(10) DEFAULT NULL,
  `id_student` int(10) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `main_invoice_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_main_invoice` int(10) DEFAULT NULL,
  `id_fee_item` int(10) DEFAULT NULL,
  `price` int(20) DEFAULT 0,
  `quantity` int(20) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(20) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




CREATE TABLE `receipt` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `type` varchar(50) DEFAULT '',
  `id_category` int(20) DEFAULT 0,
  `id_student` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT 0,
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `currency` varchar(520) DEFAULT '',
  `receipt_date` datetime DEFAULT current_timestamp,
  `approval_status` varchar(50) DEFAULT '',
  `status` int(2) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp,
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `receipt_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_receipt` int(10) DEFAULT NULL,
  `id_main_invoice` int(10) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(10) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `course` ADD `best_selling` INT(2) NULL DEFAULT '0' AFTER `code`, ADD `best_trending` INT(2) NULL DEFAULT '0' AFTER `best_selling`;


ALTER TABLE `student` ADD `full_name` VARCHAR(1024) NULL DEFAULT '' AFTER `id`;


CREATE TABLE `student_has_course` (
  `id` int(20) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `id_student` int(20) DEFAULT NULL,
  `id_category` int(20) DEFAULT NULL,
  `id_course` int(20) DEFAULT NULL,
  `id_invoice` int(20) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `created_by` int(20) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(20) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--------------------- Update Here From Server -------------------------

ALTER TABLE `course` ADD `monthd` INT(20) NULL DEFAULT '0' AFTER `code`;
ALTER TABLE `course` CHANGE `monthd` `months` INT(20) NULL DEFAULT '0';

ALTER TABLE `student_has_course` ADD `expiry_date` DATETIME NULL DEFAULT NULL AFTER `id_invoice`;

ALTER TABLE `student_has_course` ADD `amount` FLOAT(20,2) NULL DEFAULT '0' AFTER `expiry_date`;



































