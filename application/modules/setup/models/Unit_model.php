<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Unit_model extends CI_Model
{
    function categoryListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('category as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function unitListSearch($data)
    {
        $this->db->select('c.*, cat.name as category_name, cou.name as course_name, cou.code as course_code');
        $this->db->from('unit as c');
        $this->db->join('category as cat', 'c.id_category = cat.id','left');
        $this->db->join('course as cou', 'c.id_course = cou.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_category'] != '')
        {
            $this->db->where('c.id_category', $data['id_category']);
        }
        if ($data['id_course'] != '')
        {
            $this->db->where('c.id_course', $data['id_course']);
        }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getUnit($id)
    {
         $this->db->select('c.*');
        $this->db->from('unit as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewUnit($data)
    {
        $this->db->trans_start();
        $this->db->insert('unit', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editUnit($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('unit', $data);

        return $result;
    }

    function deleteUnit($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('unit', $data);

        return $this->db->affected_rows();
    }

    function getCourseByCategory($id_category)
    {
        $this->db->select('ac.*');
        $this->db->from('course as ac');
        $this->db->where('ac.status', '1');
        $this->db->where('ac.id_category', $id_category);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function addMarksDistribution($data)
    {
        $this->db->trans_start();
        $this->db->insert('marks_distribution', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function unitMarksDistribution($id)
    {
        $this->db->select('ac.*');
        $this->db->from('marks_distribution as ac');
        $this->db->where('ac.id_unit', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function deleteMarksDistribution($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('marks_distribution');

        return $this->db->affected_rows();
    }
}