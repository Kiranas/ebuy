<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
                $this->load->model('register_model');
        $this->load->model('login_model');

    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
            $data = array();

        if($this->input->post())
        {
                $data['name'] = $this->input->post('name');
                $data['email'] = $this->input->post('email');
                $data['password'] = $this->input->post('password');
                $data['dob'] = $this->input->post('dob');
                $data['mobile'] = $this->input->post('password');
                $this->register_model->addNewRegistration($data);


                           $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            $result = $this->login_model->loginMe($email, $password);


                            $sessionArray = array('id'=>$result->id,
                                        'name'=>$result->name,
                                        'customerLoggedIn' => TRUE
                                );
                $this->session->set_userdata($sessionArray);


            redirect('/profile/dashboard/checkout');


                
        }

                $this->loadViews('register/index',$this->global,$data,NULL);

    }


    public function duplicateemail($email = null) {


        $student_list_data = $this->register_model->duplicateCheck($email);
        if($student_list_data) {
           $return =  "1";            
        } else {
            $return =  "0";
        }
        echo $return;exit;
    }


    
}