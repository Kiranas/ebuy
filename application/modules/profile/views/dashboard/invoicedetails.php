
<div class="py-5 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Checkout </h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Checkout </a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>


  <section class="padding-y-10">
  <div class="container">
   <div class="row">
    
     <div class="col-12">
       <div class="table-responsive">
        <table class="table table-bordered">
        <thead>
          <tr>
            <th scope="col">Category Name</th>
            <th scope="col">Course</th>
            <th scope="col">Quantity</th>
            <th scope="col">Amount</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>

<?php $finaltotal = 0;

for($i=0;$i<count($listOfCourses);$i++) {
$finaltotal = $finaltotal + $listOfCourses[$i]->amount;
 ?>
          <tr>
            <td class="p-4">
            <span class="d-inline-block width-7rem border p-3 mr-3">
             <img src="<?php echo BASE_PATH;?>website/images/<?php echo $listOfCourses[$i]->image;?>" alt="">
            </span>
              <a href="#"><br/><?php echo $listOfCourses[$i]->categoryname;?></a>
            </td>
            
            <td class="p-4">
              <span class="d-inline-block width-7rem border p-3 mr-3">
             <img src="<?php echo BASE_PATH;?>website/images/<?php echo $listOfCourses[$i]->file;?>" alt="">
            </span>
              <a href="#"><br/><?php echo $listOfCourses[$i]->coursename;?></a>
            </td>
            <td class="text-center">
              <?php echo $listOfCourses[$i]->price;?>
            

            </td>
             <td class="text-center">
              <?php echo $listOfCourses[$i]->quantity;?>
            

            </td> <td class="text-center">
              <?php echo $listOfCourses[$i]->amount;?>
            

            </td>


          </tr>

<?php } ?>
          <tr>
          <td colspan="5" style="text-align: right;">
            Total: <span class="font-weight-semiBold font-size-18"><?php echo $finaltotal;?></span>
          </td>
          </tr>
        </tbody>
      </table>
      </div>
     </div> <!-- END col-12 -->
     
     <div class="col-md-6 mt-4">
       <a href="shop.html" class="btn btn-outline-light btn-icon"> <i class="ti-angle-double-left mr-2"></i> Back to shopping</a>
     </div> <!-- END col-md-6 -->
     <div class="col-md-6 mt-4 text-right">
      <?php if($totalfinal>0) { ?>
       <a href="/profile/dashboard/billing" class="btn btn-primary ml-3">Pay Now</a>
     <?php } ?>
     </div> <!-- END col-md-6 -->
   </div> <!-- END row-->  
  </div> <!-- END container-->
</section>

<script>

function deletetempcart(id) {
      $.get("/index/deletetemp/"+id, function(data, status){
             window.location.reload();
         });
}


function updatecart(id,qty) {
  $.get("/index/updateqty/"+id+"/"+qty, function(data, status){
             window.location.reload();
         });
}


  </script>
