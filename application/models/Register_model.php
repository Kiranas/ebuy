<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Register_model extends CI_Model
{
	 function addNewRegistration($data) {
	 	$result = $this->db->insert('customer', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
	 }

     function aboutus() {
         $this->db->select('fc.*');
        $this->db->from('aboutus as fc');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


	 function duplicateCheck($email) {
	 	 $this->db->select('fc.*');
        $this->db->from('customer as fc');
        $this->db->where('fc.email', $email);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
	 }

   
     function getCategory() {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function deletefromtemp($id) {
         $this->db->where('id', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }


      function updatetemp($id,$qty) {
        $this->db->where('id', $id);
        $data['quantity'] = $qty;
        $this->db->update('temp_cart', $data);
        return TRUE;
     }


     

     function getCourses($id) {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('temp_cart as tc');
        $this->db->join('product as c', 'tc.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }

     function getProductById($id) {
        $this->db->select('p.*');
        $this->db->from('product as p');

        $this->db->where('p.id_category', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


       function getCategoryById($id) {
        $this->db->select('ct.*');
        $this->db->from('category as ct');
        $this->db->where("ct.url_name like '%$id%'");

         $query = $this->db->get();
         $result = $query->result();  
         return $result;
       }
       function getAllCourses($id) {
        $this->db->select('tc.*');
        $this->db->from('product as tc');
        $this->db->where("tc.id_category in ($id)");
        
         $query = $this->db->get();

         $result = $query->result();  
         return $result;
     }


     function getCoursesById($id) {
        $this->db->select('tc.*');
        $this->db->from('product as tc');
        
        $this->db->where('tc.id', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }





}

