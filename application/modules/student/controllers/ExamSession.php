<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamSession extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_session_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_session.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_exam_set'] = $this->security->xss_clean($this->input->post('id_exam_set'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examSessionList'] = $this->exam_session_model->examSessionListSearch($formData);

            $data['examSetList'] = $this->exam_session_model->examSetListByStatus('1');
            $data['examCenterList'] = $this->exam_session_model->examCenterListByStatus('1');

            $this->global['pageCode'] = 'exam_session.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("exam_session/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_session.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_exam_set = $this->security->xss_clean($this->input->post('id_exam_set'));
                $status = $this->security->xss_clean($this->input->post('status'));


                
                $data = array(
                   'name' => $name,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'id_exam_center' => $id_exam_center,
                    'id_exam_set' => $id_exam_set,
                    'status' => $status,
                    'created_by' => $user_id
                );

                $result = $this->exam_session_model->addExamSession($data);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Session Created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Session Creation failed');
                }
                redirect('/exam/examSession/list');
            }

            $data['examSetList'] = $this->exam_session_model->examSetListByStatus('1');
            $data['examCenterList'] = $this->exam_session_model->examCenterListByStatus('1');


            $this->global['pageCode'] = 'exam_session.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam Event';
            $this->loadViews("exam_session/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_session.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examSession/list');
            }
            if($this->input->post())
            {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_exam_set = $this->security->xss_clean($this->input->post('id_exam_set'));
                $status = $this->security->xss_clean($this->input->post('status'));


                
                $data = array(
                   'name' => $name,
                    'start_date' => date('Y-m-d', strtotime($start_date)),
                    'end_date' => date('Y-m-d', strtotime($end_date)),
                    'id_exam_center' => $id_exam_center,
                    'id_exam_set' => $id_exam_set,
                    'status' => $status,
                    'updated_by' => $user_id
                );


               
                $result = $this->exam_session_model->editExamSession($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Event edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Event edit failed');
                }
                redirect('/exam/examSession/list');
            }
            $data['locationList'] = $this->exam_session_model->examCenterLocationListByStatus('1');
            $data['examCenterList'] = $this->exam_session_model->examCenterListByStatus('1');
            
            $data['getExamSession'] = $this->exam_session_model->getExamSession($id);
            $this->global['pageCode'] = 'exam_session.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->loadViews("exam_session/edit", $this->global, $data, NULL);
        }
    }


    function getCentersByLocatioin($id_location)
    {
            $results = $this->exam_session_model->getCentersByLocatioin($id_location);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }
}
