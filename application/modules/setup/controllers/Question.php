<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Question extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('question.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));

            $data['searchParam'] = $formData;
            $data['questionList'] = $this->question_model->questionListSearch($formData);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'question.list';
            $this->loadViews("question/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('question.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {
                if ($_FILES['image_file']) {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];

                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }


                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'question' => $question,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'status' => $status,
                    'created_by' => $user_id
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $this->question_model->addNewQuestion($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Question added successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Question add failed');
                }
                redirect('/setup/question/list');
            }

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');

            $this->global['pageCode'] = 'question.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->loadViews("question/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('question.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {
                if ($_FILES['image_file']) {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];
                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }


                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $data = array(
                    'question' => $question,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'status' => $status,
                    'updated_by' => $user_id
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $this->question_model->editQuestion($data, $id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Question edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question edit failed');
                }
                redirect('/setup/question/list');
            }

            $data['question'] = $this->question_model->getQuestion($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByCourse($data['question']->id_course);
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            // echo "<pre>";print_r($data);die;

            $this->global['pageTitle'] = 'Examination Management System : Edit Question';
            $this->global['pageCode'] = 'question.edit';
            $this->loadViews("question/edit", $this->global, $data, NULL);
        }
    }

    function addOptions($id = NULL)
    {
        if ($this->checkAccess('questionhasoption.add') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {

                if ($_FILES['image']) {
                    $certificate_name = $_FILES['image']['name'];
                    $certificate_size = $_FILES['image']['size'];
                    $certificate_tmp = $_FILES['image']['tmp_name'];
                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }


                $option_description = $this->security->xss_clean($this->input->post('option_description'));
                $is_correct_answer = $this->security->xss_clean($this->input->post('is_correct_answer'));

                $data = array(
                    'id_question' => $id,
                    'option_description' => $option_description,
                    'is_correct_answer' => $is_correct_answer
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $this->question_model->addQuestionHasOption($data);

                redirect('/setup/question/addOptions/' . $id);
            }

            $data['question'] = $this->question_model->getQuestion($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['questionHasOption'] = $this->question_model->getQuestionHasOption($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');

            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->global['pageCode'] = 'questionhasoption.add';
            $this->loadViews("question/question_has_option", $this->global, $data, NULL);
        }
    }

    function deleteOption($id_option)
    {
        $inserted_id = $this->question_model->deleteOption($id_option);
        echo "Success";
    }
}
