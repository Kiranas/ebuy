<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_pool_model extends CI_Model
{
    function poolListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('question_pool as c');
        if ($data['name'] != '') {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['code'] != '') {
            $likeCriteria = "(c.code  LIKE '%" . $data['code'] . "%')";
            $this->db->where($likeCriteria);
        }

        $this->db->order_by("c.id", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

   


     function getPool($id)
    {
        $this->db->select('c.*');
        $this->db->from('question_pool as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewPool($data)
    {
        $this->db->trans_start();
        $this->db->insert('question_pool', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editPool($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('question_pool', $data);

        return $result;
    }

    function deletePool($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('question_pool', $data);
        return $this->db->affected_rows();
    }

   
}
