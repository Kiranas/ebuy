<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    function roleListingCount()
    {
        $this->db->select('BaseTbl.id as roleId, BaseTbl.role');
        $this->db->from('roles as BaseTbl');
        $query = $this->db->get();
        
        return $query->num_rows();
    }

     function getCourses($id) {
        $this->db->select('tc.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('temp_cart as tc');
        $this->db->join('product as c', 'tc.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('tc.id_session', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getinvoiceDetails($id)
     { $this->db->select('mid.*,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('main_invoice_details as mid');
        $this->db->join('product as c', 'mid.id_product = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('mid.id_main_invoice', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function deletefromtempsuccess($id) {
        $this->db->where('id_session', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }

     function deletefromtemp($id) {
         $this->db->where('id', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }

     function deletefromtempBySessionid($id) {
         $this->db->where('id_session', $id);
        $this->db->delete('temp_cart');
        return TRUE;
     }


      function getCoursesByStudent($id){
        $this->db->select('s.*,inv.*');
        $this->db->from('customer as s');
        $this->db->join('main_invoice as inv', 's.id = inv.id_customer');
        $this->db->where('inv.id_customer', $id);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function getCustomerDetails($id) {
         $this->db->select('s.*');
        $this->db->from('customer as s');
        $this->db->where('s.id', $id);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getInvoicesByCourses($id){
        $this->db->select('inv.*,s.first_name,s.last_name,s.email,ct.name as categoryname, c.name as coursename,c.file,ct.image,c.best_selling,c.best_trending');
        $this->db->from('main_invoice as inv');
        $this->db->join('student as s', 'inv.id_student=s.id');
        $this->db->join('course as c', 'inv.id_course = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('inv.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
     function getInvoices($id) {
        $this->db->select('inv.*,s.first_name,s.last_name,s.email,ct.name as categoryname, c.name as coursename,c.file,ct.image');
        $this->db->from('main_invoice as inv');
        $this->db->join('student as s', 'inv.id_student=s.id');
        $this->db->join('course as c', 'inv.id_course = c.id');
        $this->db->join('category as ct', 'c.id_category = ct.id');

        $this->db->where('inv.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }


     function getReceipt($id) {
        $this->db->select('rcp.*,s.first_name,s.last_name,s.email');
        $this->db->from('receipt as rcp');
        $this->db->join('student as s', 'rcp.id_student=s.id');
        $this->db->where('rcp.id_student', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
     }
     
    
    function countryListByStatus($status)
    {
    	$this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getFeestructureByData($data)
    {
        $this->db->select('*');
        $this->db->from('fee_structure_main');
        $this->db->where('id_category', $data['id_category']);
        $this->db->where('id_course', $data['id_course']);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getCourseByData($data)
    {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('id_category', $data['id_category']);
        $this->db->where('id', $data['id_course']);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }


    function getFeestructureDetailsByIdFeeStructureMain($id)
    {
        $this->db->select('fes.*');
        $this->db->from('fee_structure as fes');
        $this->db->join('fee_setup as fs','fes.id_fee_item = fs.id');
        // $this->db->where('fes.id_category', $data['id_category']);
        // $this->db->where('fes.id_course', $data['id_course']);
        $this->db->where('fes.id_fee_structure', $id);
        $this->db->order_by("fes.id", "DESC");
        $query = $this->db->get();
        return $query->result();
    }


    function generateMainInvoiceNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('main_invoice as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
        $jrnumber = $number = "INV" .(sprintf("%'06d", $count)). "/" . $Year;
        return $jrnumber;
    }

    function addInvoice($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function getpreviousBilling($id)
    {
        $this->db->trans_start();
        $this->db->from('billing_address');
        $this->db->where('id_customer', $id);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getBillingDetailsById($id)
    {
        $this->db->trans_start();
        $this->db->from('billing_address');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getpreviousShipping($id)
    {
        $this->db->trans_start();
        $this->db->from('shipping_address');
        $this->db->where('id_customer', $id);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function insertBilling($data)
    {

                $result = $this->db->insert('billing_address', $data);
        $insert_id = $this->db->insert_id();



        return $insert_id;
    }
    function insertShipping($data)
    {
        $this->db->trans_start();
        $this->db->insert('shipping_address', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    

    function addStudentHasCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_has_course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addInvoiceDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('main_invoice_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editInvoice($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('main_invoice', $data);

        return $this->db->affected_rows();
    }
    function updateProfile($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('customer', $data);

        return $this->db->affected_rows();
    }

    function getInvoice($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice');
        $this->db->where('id', $id);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function getInvoiceForReceiptAdd($id)
    {
        $this->db->select('*');
        $this->db->from('main_invoice');
        $this->db->where('id', $id);
        $this->db->where('balance_amount !=', 0);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function generateReceiptNumber()
    {
        $year = date('y');
        $Year = date('Y');
        
        $this->db->select('j.*');
        $this->db->from('receipt as j');
        $this->db->order_by("id", "desc");
        $query = $this->db->get();
        $result = $query->num_rows();

 
        $count= $result + 1;
       $jrnumber = $number = "REC" .(sprintf("%'06d", $count)). "/" . $Year;
       return $jrnumber;
    }

    function addReceipt($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addReceiptDetails($data)
    {
        $this->db->trans_start();
        $this->db->insert('receipt_details', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function getCoursesById($id) {
        $this->db->select('ct.*');
        $this->db->from('product as ct');
        $this->db->where('ct.id', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
     }


} 