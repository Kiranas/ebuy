<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Fee_setup_model extends CI_Model
{
    function feeCategoryListByStatus($status)
    {
        $this->db->select('ac.*');
        $this->db->from('fee_category as ac');
        $this->db->where('ac.status', $status);
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function feeSetupList()
    {
        $this->db->select('fs.*, fc.name as fee_category, ac.code as account_code, ac.type as account_type');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        $this->db->join('financial_account_code as ac', 'fs.account_code = ac.id','left');
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getFeeSetup($id)
    {
       $this->db->select('fs.*, fc.name as fee_category');
        $this->db->from('fee_setup as fs');
        $this->db->join('fee_category as fc', 'fs.id_fee_category = fc.id');
        // $this->db->join('account_code as ac', 'fs.id_account_code = ac.code');
        $this->db->where('fs.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewFeeSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('fee_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editFeeSetup($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('fee_setup', $data);
        return TRUE;
    }

    function financialAccountCodeListByStatus($status)
    {
        // $query = 'SELECT ac.* FROM  account_code ac';
        // $sql = $this->db->query($query);
        // $result = $sql->result();

        // $this->db->select('ac.*, CONCAT(ac.code, '.', ac.name) AS code_name');
        $this->db->select('ac.*');
        $this->db->from('financial_account_code as ac');
        $this->db->where('ac.status', '1');
        $this->db->order_by("ac.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function feeSetupListByStatus($status)
    {
        $this->db->select('fs.*');
        $this->db->from('fee_setup as fs');
        $this->db->where('fs.status', $status);
        $this->db->order_by("fs.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}

