<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Fee Structure</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_fee_setup" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Fee Structure</h4>
          </div>

            <div class="form-container">


                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Category <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_category" id="id_category" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($categoryList))
                            {
                                foreach ($categoryList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>"
                                <?php 
                                if($record->id == $feeStructure->id_category)
                                {
                                    echo "selected";
                                } ?>>
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>




                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <span id="view_course">
                                <select class="form-control" id='id_course' name='id_course' disabled>
                                   <option value="">Select</option>
                                <?php
                                if (!empty($courseList))
                                {
                                    foreach ($courseList as $record)
                                    {?>
                                 <option value="<?php echo $record->id;  ?>"
                                    <?php 
                                    if($record->id == $feeStructure->id_course)
                                    {
                                        echo "selected";
                                    } ?>>
                                    <?php echo $record->code . " - " . $record->name;?>
                                 </option>
                                <?php
                                    }
                                }
                                ?>
                                  </select>
                            </span>
                          </div>
                        </div>
                    </div>

                </div>


                <!-- <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Total Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                          <input type="number" class="form-control" id="total_amount" name="total_amount" placeholder="Amount" readonly="">
                          </div>
                        </div>
                    </div>

                    
                </div> -->


                  
                

            </div>                                
        </div>
    </form>


    <br>


    <form id="form_detail" action="" method="post">

          <div class="page-container">

            <div>
              <h4 class="form-title">Fee Structure details</h4>
            </div>

              <div class="form-container">


                  <div class="row">


                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Fee Item</label>
                          <div class="col-sm-8">
                            <select name="id_fee_item" id="id_fee_item" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($feeSetupList))
                            {
                                foreach ($feeSetupList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->code . " - " . $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                      </div>   

                      <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Amount <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                          <input type="number" class="form-control" id="details_amount" name="details_amount" placeholder="Amount">
                          </div>
                        </div>
                      </div>

                  </div>       


                    
                  <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="addFeeStructureDetails()">Save</button>
                          <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                    </div>

                  </div> 

              </div>                                
          </div>


          <?php

        if(!empty($getFeeStructureDetailsByIdFeeStructure))
        {
            ?>
            <br>

            <div class="form-container">
                    <h4 class="form-group-title">Fee Structure Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                            <th>Fee Item</th>
                            <th>Amount</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($getFeeStructureDetailsByIdFeeStructure);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $getFeeStructureDetailsByIdFeeStructure[$i]->fee_item_code . " - " . $getFeeStructureDetailsByIdFeeStructure[$i]->fee_item_name; ?></td>
                            <td><?php echo $getFeeStructureDetailsByIdFeeStructure[$i]->amount; ?></td>
                            <td class="text-center">
                            <a onclick="deleteFeeStructureDetails(<?php echo $getFeeStructureDetailsByIdFeeStructure[$i]->id; ?>)">Delete</a>
                            </td>
                             </tr>
                          <?php 
                          $total = $total + $getFeeStructureDetailsByIdFeeStructure[$i]->amount;
                      } 
                      ?>
                            <tr>
                            <td></td>
                            <td>Total : </td>
                            <td><?php echo $total; ?></td>
                            <td class="text-center">
                            </td>
                             </tr>
                        </tbody>
                    </table>
                  </div>

                </div>

        <?php
        
        }
         ?>

      </form>



</main>

<script>
    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });


    $('select').select2();


    function reloadPage()
    {
      window.location.reload();
    }

    function getCourseByCategory(id)
    {
        $.get("/finance/feeStructure/getCourseByCategory/"+id, function(data, status){
       
            $("#view_course").html(data);
        });
    }


    function addFeeStructureDetails()
    {
        if($('#form_detail').valid())
        {

        var tempPR = {};
        tempPR['id_fee_item'] = $("#id_fee_item").val();
        tempPR['amount'] = $("#details_amount").val();
        tempPR['id_fee_structure'] = <?php echo $feeStructure->id ?>;
        tempPR['id_category'] = <?php echo $feeStructure->id_category ?>;
        tempPR['id_course'] = <?php echo $feeStructure->id_course ?>;
        tempPR['status'] = 1;

            $.ajax(
            {
               url: '/finance/feeStructure/addFeeStructureDetails',
                type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
                // $("#view_fee_details").html(result);

                // var total_master = $("#calculated_total_amount").val();
                // $("#total_amount").val(total_master);
               }
            });
        }
    }


    function deleteFeeStructureDetails(id) {
        // alert(id);
         $.ajax(
            {
               url: '/finance/feeStructure/deleteFeeStructureDetails/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    window.location.reload();
                    // $("#view_fee_details").html(result);
               }
            });
    }


    $(document).ready(function() {
        $("#form_detail").validate({
            rules: {
                id_fee_item: {
                    required: true
                },
                details_amount: {
                    required: true
                }
            },
            messages: {
                id_fee_item: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                details_amount: {
                    required: "<p class='error-text'>Amount Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    $(document).ready(function() {
        $("#form_fee_setup").validate({
            rules: {
                id_category: {
                    required: true
                },
                id_course: {
                    required: true
                },
                total_amount: {
                    required: true
                }
            },
            messages: {
                id_category: {
                    required: "<p class='error-text'>Select Category</p>",
                },
                id_course: {
                    required: "<p class='error-text'>Select Module</p>",
                },
                total_amount: {
                    required: "<p class='error-text'>Add Details For Total Amount</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    


</script>