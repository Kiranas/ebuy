<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_model extends CI_Model
{
    function categoryListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('category as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListSearch($data)
    {
        $this->db->select('c.*, cat.name as category_name');
        $this->db->from('product as c');
        $this->db->join('category as cat', 'c.id_category = cat.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCourse($id)
    {
         $this->db->select('c.*');
        $this->db->from('product as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('product', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editCourse($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('product', $data);

        return $this->db->affected_rows();
    }

    function deleteCourse($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('product', $data);

        return $this->db->affected_rows();
    }
}
