 

<?php 

error_reporting(0);
$this->load->model('register_model');
$categoryList = $this->register_model->getCategory();

?>
   
   
   
   
<section class="pt-2 paddingBottom-100 bg-light-v2">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
    <div id="accordion"> 
       
        <div class="card card-body accordion-item marginTop-30">
         <a href="#acc1" class="accordion__title h6 mb-0" data-toggle="collapse" aria-expanded="true">
           Filter by Food Product
           
         </a>
          <div id="acc1" class="collapse show" data-parent="#accordion">
           <div class="mt-4">

            <?php for($i=0;$i<count($categoryList);$i++){ 
               $name = $categoryList[$i]->name;
               $id = $categoryList[$i]->id;
               $durl_name = $categoryList[$i]->url_name;
                ?>
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="allproducts" value="<?php echo $id;?>"
                <?php if($url_name==$durl_name) { echo  "checked=checked";} ?>>
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel"><?php echo  $name;?></span>
              </label>
             </p>
            <?php }?> 
            

           </div>
          </div>
        </div> <!-- END accordion-item--> 
       
       <!--  <div class="card card-body accordion-item marginTop-30">
         <a href="#acc2" class="accordion__title h6 mb-0 collapsed" data-toggle="collapse" aria-expanded="true">
           Filter by Author
           <span class="accordion__icon float-right small mx-2 mt-1">
            <i class="ti-angle-down"></i>
            <i class="ti-angle-up"></i>
          </span>
         </a>
          <div id="acc2" class="collapse" data-parent="#accordion">
          <div class="mt-4">
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="checkbox">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">All Authors</span>
              </label>
             </p>
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="checkbox">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">Michael John</span>
              </label>
             </p>
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="checkbox">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">Thomas Rang</span>
              </label>
             </p>
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="checkbox">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">Gigy Sayfan</span>
              </label>
             </p>
             <p class="mb-2">
              <label class="ec-checkbox">
                <input type="checkbox" name="checkbox">
                <span class="ec-checkbox__control"></span>
                <span class="ec-checkbox__lebel">Daniel Brain</span>
              </label>
             </p>

           </div>
          </div>
        </div>
       

        <div class="card card-body accordion-item marginTop-30">
         <a href="#acc3" class="accordion__title h6 mb-0 collapsed" data-toggle="collapse" aria-expanded="true">
           Filter by Price
           <span class="accordion__icon float-right small mx-2 mt-1">
            <i class="ti-angle-down"></i>
            <i class="ti-angle-up"></i>
          </span>
         </a>
          <div id="acc3" class="collapse" data-parent="#accordion">
           <div class="marginTop-80">
             <div id="rangeSlide1" class="ec-range-slider"></div>
             <div class="d-flex align-items-center mt-4">
               <input type="text" class="form-control px-2" id="noUiSliders_1_input">
               <span class="mx-2">-</span>
               <input type="text" class="form-control px-2" id="noUiSliders_1_1_input">
             </div>
           </div>
          </div>
        </div>  -->
        
      </div> <!-- END accordion-->

      </div> <!-- END col-md-3 -->
      
      <div class='col-md-9'>
        <div id="productid"></div>
      </div>
    </div> <!-- END row-->
  </div> <!-- END container-->
</section>
 
<footer class="site-footer" style="color:white;">
  <div class="footer-top bg-dark text-white-0_6 pt-5 paddingBottom-100">
    <div class="container"> 
      <div class="row">

        <div class="col-lg-3 col-md-6 mt-5">
         <img src="assets/images/ezzibuy_logo_white.svg" alt="Logo" style="height: 150px;">
         <div class="margin-y-40">
           <p>
            Store of Homemade Happiness!!!!!
          </p>
         </div>
          <ul class="list-inline"> 
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-facebook"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-twitter"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-linkedin"> </i></a></li>
            <li class="list-inline-item"><a class="iconbox bg-white-0_2 hover:primary" href=""><i class="ti-pinterest"></i></a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 mt-5">
          <h4 class="h5 text-white">Contact Us</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-3"><i class="ti-headphone mr-3"></i><a href="tel:+8801740411513">93535 04127 </a></li>
            <li class="mb-3"><i class="ti-email mr-3"></i><a href="mailto:support@educati.com">contactus@ezzibuy.in</a></li>
            <li class="mb-3">
             <div class="media">
              <i class="ti-location-pin mt-2 mr-3"></i>
              <div class="media-body">
                <span> #451/A, 1st Cross, 2nd Main Road,Yelahanka, Bangalore- 560064</span>
              </div>
             </div>
            </li>
          </ul>
        </div>

        <div class="col-lg-2 col-md-6 mt-5">
          <h4 class="h5 text-white">Quick links</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <ul class="list-unstyled marginTop-40">
            <li class="mb-2"><a href="#">About Us</a></li>
            <li class="mb-2"><a href="#">Privacy Policy</a></li>
            <li class="mb-2"><a href="#">Terms and Condition</a></li>
          </ul>
        </div>

        <div class="col-lg-4 col-md-6 mt-5">
          <h4 class="h5 text-white">Newslatter</h4>
          <div class="width-3rem bg-primary height-3 mt-3"></div>
          <div class="marginTop-40">
            <p class="mb-4">Subscribe to get update and information. Don't worry, we won't send spam!</p>
            <form class="marginTop-30" action="#" method="POST">
              <div class="input-group">
                <input type="text" placeholder="Enter your email" class="form-control py-3 border-white" required="">
                <div class="input-group-append">
                  <button class="btn btn-primary" type="submit">Subscribe</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        
      </div> <!-- END row-->
    </div> <!-- END container-->
  </div> <!-- END footer-top-->


</footer> <!-- END site-footer -->
<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>


<script>
  $( document ).ready(function() {

     getproductlist();
});

  function buynow(id,amount)
    {
      var quantity = 1;
        $.get("/coursedetails/tempbuynow/"+id+"/"+amount+"/"+quantity, function(data, status){
             parent.location= "<?php echo BASE_PATH;?>/index/checkout";
         });
    }

    function getproductlist(){


      var values = [];
    {
      $('#acc1 :checked').each(function() {
        //if(values.indexOf($(this).val()) === -1){
        // values=values+parseFloat(($(this).val()));
        values.push($(this).val());

        // }
      });
      $.post("/product/getproductlist/",
      {
        categories: values
      },
      function(data, status){
        $("#productid").html(data);
      });
    }

    }


    $('#acc1').change(function() {
      getproductlist();
  });

  </script>