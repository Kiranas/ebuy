<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class AlumniLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alumni_login_model');
    }

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        // echo "<Pre>";print_r("dasdsa");exit();
        $this->checkAlumniLoggedIn();
    }
    

    function checkAlumniLoggedIn()
    {
        $isAlumniLoggedIn = $this->session->userdata('isAlumniLoggedIn');
        
        if(!isset($isAlumniLoggedIn) || $isAlumniLoggedIn != TRUE)
        {
            $this->load->view('alumni_login');
        }
        else
        {
            redirect('alumni/profile');
        }
    }

    public function alumniLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->alumni_login_model->loginAlumni($email, $password);
            
            
            if(!empty($result))
            {
                if($result->applicant_status == 'Graduated')
                {
                    
                $lastLogin = $this->alumni_login_model->alumniLastLoginInfo($result->id_student);

                if($lastLogin == '')
                {
                    $alumni_login = date('Y-m-d h:i:s');
                }
                else
                {
                    $alumni_login = $lastLogin->created_dt_tm;
                }

                $sessionArray = array('id_alumni'=>$result->id_student,
                                        'alumni_name'=>$result->student_name,
                                        'alumni_email_id'=>$result->email_id,
                                        'alumni_nric'=>$result->nric,
                                        'alumni_id_intake'=>$result->id_intake,
                                        'alumni_id_program'=>$result->id_program,
                                        'alumni_id_qualification'=>$result->id_degree_type,
                                        'alumni_last_login'=> $alumni_login,
                                        'isAlumniLoggedIn' => TRUE
                                );
        // echo "<Pre>";print_r($sessionArray);exit();

                $this->session->set_userdata($sessionArray);

                unset($sessionArray['id_alumni'], $sessionArray['isAlumniLoggedIn'], $sessionArray['alumni_last_login']);

                $loginInfo = array("id_student"=>$result->id_student, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_alumni_session_id", md5($uniqueId));


                $this->alumni_login_model->addAlumniLastLogin($loginInfo);

                // echo "<Pre>"; print_r($this->session->userdata());exit();
                // echo md5($uniqueId);exit();
                redirect('/alumni/profile');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }
}

?>