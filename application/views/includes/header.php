<?php 

error_reporting(0);
$this->load->model('register_model');
$categoryList = $this->register_model->getCategory();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    
    <!-- Title-->
    <title>Complete education theme for School, College, University, E-learning</title>
    
    <!-- SEO Meta-->
    <meta name="description" content="Education theme by EchoTheme">
    <meta name="keywords" content="HTML5 Education theme, responsive HTML5 theme, bootstrap 4, Clean Theme">
    <meta name="author" content="education">
    
    <!-- viewport scale-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    
            
    <!-- Favicon and Apple Icons-->
    <link rel="icon" type="image/x-icon" href="<?php echo BASE_PATH;?>/website/img/favicon/favicon.ico">
    <link rel="shortcut icon" href="<?php echo BASE_PATH;?>website/img/favicon/114x114.png">
    <link rel="apple-touch-icon-precomposed" href="<?php echo BASE_PATH;?>website/img/favicon/96x96.png">
    
    
    <!--Google fonts-->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Maven+Pro:400,500,700%7CWork+Sans:400,500">
    
    
    <!-- Icon fonts -->
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>/website/fonts/fontawesome/css/all.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/fonts/themify-icons/css/themify-icons.css">
    
    
    <!-- stylesheet-->    
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/vendors.bundle.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/style.css">
    <link rel="stylesheet" href="<?php echo BASE_PATH;?>website/css/main.css">
    
  </head>
  
  <body>
   

  <header class="site-header bg-dark text-white-0_5">        
    <div class="container">
      <div class="row align-items-center justify-content-between mx-0" style="color:white;height:35px;">
       <marquee behavior="scroll" direction="left">Here is some scrolling text... right to left!</marquee>


       <!--  <?php if($this->session->userdata['id']) { ?> 
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-left border-white-0_1">
            <a href="/profile/dashboard/index">Welcome <?php echo $this->session->userdata['name'];?></a>
          </li>
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-white-0_1">
            <a href="/index/logout">Logout</a>
          </li>
        </ul>
      <?php } else   { ?> 
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-left border-white-0_1">
            <a href="/login">Login</a>
          </li>
          <li class="list-inline-item mr-0 p-md-3 p-2 border-right border-white-0_1">
            <a href="/register">Register</a>
          </li>
        </ul>
      <?php } ?> 
 -->
      </div> <!-- END END row-->
    </div> <!-- END container-->
  </header><!-- END site header-->
  
  

  <nav class="ec-nav sticky-top bg-white">
  <div class="container">
    <div class="navbar p-0 navbar-expand-lg">
      <div class="navbar-brand">
        <a class="logo-default" href="/"><img alt="Ezzibuy" src="/assets/images/ezzibuy_logo.svg"></a>
      </div>
      <span aria-expanded="false" class="navbar-toggler ml-auto collapsed" data-target="#ec-nav__collapsible" data-toggle="collapse">
        <div class="hamburger hamburger--spin js-hamburger">
          <div class="hamburger-box">
            <div class="hamburger-inner"></div>
          </div>
        </div>
      </span>
      <div class="collapse navbar-collapse when-collapsed" id="ec-nav__collapsible">
        <ul class="nav navbar-nav ec-nav__navbar ml-auto">


<?php for($i=0;$i<count($categoryList);$i++){ 
  $url = $categoryList[$i]->url_name;
   $name = $categoryList[$i]->name; 

$categoryId = $categoryList[$i]->id;

$productList = $this->register_model->getProductById($categoryId);


     ?>


 <li class="nav-item nav-item__has-dropdown">
                <a class="nav-link dropdown-toggle" href="/product/index/<?php echo $url;?>" ><?php echo $name;?></a>
                <ul class="dropdown-menu">

                <?php for($p=0;$p<count($productList);$p++) { 
 $productname = $productList[$p]->name; 
 $productId = $productList[$p]->id; 


?>

                  <li><a href="<?php echo BASE_PATH;?>coursedetails/index/<?php echo $productId;?>" class="nav-link__list"><?php echo $productname;?></a></li>

               <?php }?>
                 
                 
                </ul>
            </li>

  <?php } ?> 

         

        </ul>
      </div>
        
    </div>
  </div> <!-- END container-->      
  </nav> <!-- END ec-nav -->   