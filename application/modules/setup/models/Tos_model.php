<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tos_model extends CI_Model
{
    function tosListSearch($data)
    {
        $this->db->select('c.*');
        $this->db->from('tos as c');
        if (!empty($data['name'])) {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%'";
            $this->db->where($likeCriteria);
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addNewTos($main_data,$detail_data)
    {
        $this->db->trans_start();
        $this->db->insert('tos', $main_data);
        $insert_id = $this->db->insert_id();
        foreach($detail_data as $row){
            $row['id_tos'] = $insert_id;
            $this->db->insert('tos_details', $row);
        }
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTos($id)
    {
        $this->db->select('c.*');
        $this->db->from('tos as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
    function getTosDetails($id)
    {
        $this->db->select('c.*,course.name as courseName,topic.name as topicName,bloom_taxonomy.name as taxonomyName,difficult_level.name as levelName');
        $this->db->from('tos_details as c');
        $this->db->join('course', 'c.id_course = course.id');
        $this->db->join('topic', 'c.id_topic = topic.id');
        $this->db->join('bloom_taxonomy', 'c.id_bloom_taxonomy = bloom_taxonomy.id');
        $this->db->join('difficult_level', 'c.id_difficult_level = difficult_level.id');
        $this->db->where('c.id_tos', $id);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

   

    function editTos($main_data, $detail_data)
    {
        $this->db->trans_start();
        $this->db->where('id', $main_data['id']);
        $result = $this->db->update('tos', $main_data);

        foreach($detail_data as $row){
            $this->db->where('id_details', $row['id_details']);
            $result = $this->db->update('tos_details', $row);
        }
        $this->db->trans_complete();
        return $result;
    }

    function deleteTos($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tos', $data);
        return $this->db->affected_rows();
    }

   

    function getTosQuestions($pools)
    {
        $query = $this->db->query('select count(*) as QuestionCount,course.name as CourseName,question.id_course,question.id_pool,topic.name as TopicName,question.id_topic,bloom_taxonomy.name as TaxonomyName,question.id_bloom_taxonomy,difficult_level.name LevelName,question.id_difficult_level from question 
        inner join course on course.id = question.id_course
        inner join topic on topic.id = question.id_topic
        inner join bloom_taxonomy on bloom_taxonomy.id = question.id_bloom_taxonomy
        inner join difficult_level on difficult_level.id = question.id_difficult_level
        where question.id_pool in (' . $pools . ')
        GROUP BY question.id_course,question.id_topic,question.id_bloom_taxonomy,question.id_difficult_level');
        $result = $query->result();
        return $result;
    }
}
