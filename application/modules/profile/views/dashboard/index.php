  
<div class="py-2 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Profile Details</h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="#">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="../dashboard/index"> Profile Details</a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>
<form method="POST" action="<?php echo base_url(); ?>profile/dashboard/index">

<section class="padding-y-10">
  <div class="container">

   <div class="row">
        <div class="col-md-12 order-md-1">
            <div class="row">

              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control" id="name" name='name' placeholder="" value="<?php echo $customerDetails->name;?>" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="firstName">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="" value="<?php echo $customerDetails->email;?>" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>

              <div class="col-md-6 mb-3">
                <label for="firstName">Mobile</label>
                <input type="text" class="form-control" id="mobile" name="mobile" placeholder="" value="<?php echo $customerDetails->mobile;?>" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
                <div class="col-md-6 mb-3">
                <label for="firstName">Date of Birth</label>
                <input type="text" class="form-control" id="dob" name="dob" placeholder="" value="<?php echo $customerDetails->dob;?>" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>


            </div>

           
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">Updated</button>
        </div>
      </div>
  </div> <!-- END container-->
</section>

</form>




<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> <!-- END site-footer -->


<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>

<script>
  function getStateByCountry(id)
    {
        $.get("/profile/dashboard/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
        });
    }


  function buynow(id)
  {
      var id = 1;
      $.get("/coursedetails/tempbuynow/"+id, function(data, status){
           console.log(data);
           parent.location='../login';
      });
  }

  </script>