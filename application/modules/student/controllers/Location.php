<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Location extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_location_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('location.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $name = $this->security->xss_clean($this->input->post('name'));
            $data['searchName'] = $name;
            $data['locationList'] = $this->exam_center_location_model->locationListSearch($name);
            $this->global['pageTitle'] = 'Examination Management System : Location List';
            $this->global['pageCode'] = 'location.list';
            $this->loadViews("location/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('location.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
               
                $result = $this->exam_center_location_model->addNewLocation($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Location created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Location creation failed');
                }
                redirect('/exam/location/list');
            }
            $this->global['pageTitle'] = 'Examination Management System : Add Location';
            $this->global['pageCode'] = 'location.add';
            $this->loadViews("location/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('location.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/location/list');
            }
            if($this->input->post())
            {
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );

                $result = $this->exam_center_location_model->editLocation($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Location edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Location edit failed');
                }
                redirect('/exam/location/list');
            }
            $data['location'] = $this->exam_center_location_model->getLocation($id);
            $this->global['pageCode'] = 'location.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Location';
            $this->loadViews("location/edit", $this->global, $data, NULL);
        }
    }
}
