<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Currency_rate_setup_model extends CI_Model
{
    function currencyList()
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function currencyListSearch($data)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        if (!empty($data['name']))
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%' or code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getCurrencySetup($id)
    {
        $this->db->select('*');
        $this->db->from('currency_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addCurrencyRateSetup($data)
    {
        $this->db->trans_start();
        $this->db->insert('currency_rate_setup', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editCurrency($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('currency_setup', $data);
        return TRUE;
    }

    function editCurrencyNot($data, $id)
    {
        $this->db->where('id !=', $id);
        $this->db->update('currency_setup', $data);
        return TRUE;
    }

    function getCurrencyRateSetup($id_currency)
    {
        $this->db->select('*');
        $this->db->from('currency_rate_setup');
        $this->db->where('id_currency', $id_currency);
        $query = $this->db->get();
        return $query->result();
    }

    function deleteRateSetup($id)
    {
        $this->db->where('id !=', $id);
        $this->db->delete('currency_rate_setup');
        return TRUE;
    }
}

