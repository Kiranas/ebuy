<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FeeStructure extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('fee_structure_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('fee_structure.list') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_category'] = $this->security->xss_clean($this->input->post('id_category'));
            $data['searchParam'] = $formData;
            
            //echo "<Pre>"; print_r($data);exit;
            $data['feeStructureList'] = $this->fee_structure_model->feeStructureListSearch($formData);

            $data['categoryList'] = $this->fee_structure_model->categoryListByStatus('1');
            $data['courseList'] = $this->fee_structure_model->courseListByStatus('1');

            $this->global['pageTitle'] = 'Examination Management System : Fee Structure List';
            $this->global['pageCode'] = 'fee_structure.list';

            $this->loadViews("fee_structure/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('fee_structure.add') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $id_session = $this->session->my_session_id;
            $id_user = $this->session->userId;

            if($this->input->post())
            {
            
                $id_category = $this->security->xss_clean($this->input->post('id_category'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));

            
                $data = array(
                    'id_category' => $id_category,
                    'id_course' => $id_course,
                    'amount' => $total_amount,
                    'id_currency' => 1,
                    'status' => 1
                );
                //echo "<Pre>"; print_r($data);exit;

                $inserted_id = $this->fee_structure_model->addFeeStructureMain($data);
                if ($inserted_id > 0)
                {
                    $this->session->set_flashdata('success', 'Fee Structure added successfully');

                    $data['id'] = $inserted_id;
                    
                    $moved_details = $this->fee_structure_model->moveTempToDetails($data);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Fee Structure add failed');
                }
                redirect('/finance/feeStructure/list');
            }
            else
            {
                $deleted_temp = $this->fee_structure_model->deleteTempFeeStructureDetailsByIdSession($id_session);
            }

            $data['feeSetupList'] = $this->fee_structure_model->feeSetupListByStatus('1');
            $data['categoryList'] = $this->fee_structure_model->categoryListByStatus('1');


            $this->global['pageTitle'] = 'Examination Management System : Add Sponsor';
            $this->global['pageCode'] = 'fee_structure.add';
            
            $this->loadViews("fee_structure/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('fee_structure.edit') == 0)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/finance/feeStructure/list');
            }
            if($this->input->post())
            {
                $total_amount = $this->security->xss_clean($this->input->post('total_amount'));

            
                $data = array(
                    'amount' => $total_amount
                );

                //echo "<Pre>"; print_r($data);exit;
                $result = $this->fee_structure_model->editFeeStructure($data,$id);
                redirect('/finance/feeStructure/list');
            }

            $data['feeSetupList'] = $this->fee_structure_model->feeSetupListByStatus('1');
            $data['categoryList'] = $this->fee_structure_model->categoryListByStatus('1');
            $data['courseList'] = $this->fee_structure_model->courseListByStatus('1');
            
            $data['feeStructure'] = $this->fee_structure_model->getFeeeStructure($id);
            $data['getFeeStructureDetailsByIdFeeStructure'] = $this->fee_structure_model->getFeeStructureDetailsByIdFeeStructure($id);

            $this->global['pageTitle'] = 'Examination Management System : Edit Sponsor';
            $this->global['pageCode'] = 'fee_structure.edit';

                // echo "<Pre>"; print_r($data['categoryList']);exit;
            

            $this->loadViews("fee_structure/edit", $this->global, $data, NULL);
        }
    }

    function getCourseByCategory($id)
    {
        $results = $this->fee_structure_model->getCourseByCategory($id);

        // echo "<Pre>"; print_r($programme_data);exit;
     //    $table="   
     //        <script type='text/javascript'>
     //             $('select').select2();
     //         </script>
     // ";

        $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_course' id='id_course' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        // $id = $results[$i]->id_procurement_category;
        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $code = $results[$i]->code;
        $table.="<option value=" . $id . ">" . $code . " - " . $name .
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }

    function saveTempFeeStructure()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $id_session = $this->session->my_session_id;
        $tempData['id_session'] = $id_session;

        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->fee_structure_model->saveTempFeeStructure($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;

        $data = $this->displayTempData();
        
        echo $data;        
    }

    function displayTempData()
    {
        $id_session = $this->session->my_session_id;
        
        $temp_details = $this->fee_structure_model->getTempFeeStructureDetailsByIdSession($id_session); 
        
        // echo "<Pre>";print_r($temp_details);exit;

        if(!empty($temp_details))
        {

        $table = "
        <div class='custom-table'>
        <table  class='table' id='list-table'>
                <thead>
                  <tr>
                    <th>Sl. No</th>
                    <th>Fee Item</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
                </thead>";

                    $total_amount = 0;
                
                    for($i=0;$i<count($temp_details);$i++)
                    {
                    $id = $temp_details[$i]->id;
                    $fee_item_code = $temp_details[$i]->fee_item_code;
                    $fee_item_name = $temp_details[$i]->fee_item_name;
                    $amount = $temp_details[$i]->amount;

                 

                    $j = $i+1;
                        $table .= "
                        <tbody>
                        <tr>
                            <td>$j</td>
                            <td>$fee_item_code - $fee_item_name</td>                         
                            <td>$amount</td>
                            <td>
                                <a class='btn btn-sm btn-edit' onclick='deleteTempFeeStructureDetails($id)'>Delete</a>
                            </td>
                        </tr>";
                        $total_amount = $total_amount + $amount;
                    }
            // <span onclick='deleteTempProposalHasProgramme($id)'>Delete</a>                            
            $table.= "
            <tr>
                <td></td>
                <td>Total Amount</td>                         
                <td>
                <input type='hidden' id='calculated_total_amount' name='calculated_total_amount' value='$total_amount'>
                $total_amount</td>
                <td></td>
            </tr>

            </tbody>
            </table>
            </div>";
        }
        else
        {
            $table="";
        }
        return $table;
    }

    function deleteTempFeeStructureDetails($id)
    {
        $inserted_id = $this->fee_structure_model->deleteTempFeeStructureDetails($id);
        if($inserted_id)
        {
            $data = $this->displayTempData();
            echo $data;  
        }
    }

    function addFeeStructureDetails()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        // echo "<Pre>";print_r($tempData);exit;

        $inserted_id = $this->fee_structure_model->addFeeStructureDetails($tempData);
        // echo "<Pre>";print_r($inserted_id);exit;
        
        echo 'sucess';exit;
    }

    function deleteFeeStructureDetails($id)
    {
        $inserted_id = $this->fee_structure_model->deleteFeeStructureDetails($id);
        echo 'sucess';exit;
    }
}