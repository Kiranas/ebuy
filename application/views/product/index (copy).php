 

<section class="padding-y-60 bg-light-v2">
  <div class="container">

    <div class="row">
    <div class="col-12 text-center text-white mb-5">
        <h3 style='color:black;'>
          Browse <?php echo $categoryDetails[0]->name;?>
        </h3>
        <div class="width-3rem height-4 rounded bg-primary mx-auto"></div>
      </div>
    </div>
    <div class="row">


      <?php for($m=0;$m<count($courseList);$m++) { ?>
      <div class="col-lg-4 col-md-6 marginTop-30">
        <div href="<?php echo BASE_PATH?>coursedetails/index/<?php echo $courseList[$m]->id;?>" class="card height-100p text-gray shadow-v1">
          <img class="card-img-top" src="<?php echo BASE_PATH;?>website/images/<?php echo $courseList[$m]->file;?>" alt="">
          <div class="card-body">

            <?php if($courseList[$m]->best_selling=='1') { ?>
           <span class="badge position-absolute top-0 bg-success text-white" data-offset-top="-13">
             Best selling
           </span>
         <?php } ?> 

          <?php if($courseList[$m]->best_trending=='1') { ?>
            <span class="badge position-absolute top-0 bg-danger text-white" data-offset-top="-13">
             Trending
           </span>
         <?php } ?> 


            <a href="<?php echo BASE_PATH?>coursedetails/index/<?php echo $courseList[$m]->id;?>" class="h5">
              <?php echo $courseList[$m]->name;?>
            </a>
            
            <p class="mb-0">
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <i class="fas fa-star text-warning"></i>
              <span class="text-dark">5</span>
              <span>(4578)</span>
            </p>
          </div>
          <div class="card-footer media align-items-center justify-content-between">
            <ul class="list-unstyled mb-0">
              <li class="mb-1">
                <i class="ti-headphone small mr-2"></i>
                46 lectures
              </li>
              <li class="mb-1">
                <i class="ti-time small mr-2"></i>
                27.5 hours
              </li>
            </ul>
            <h4 class="h5 text-right">
              <span class="text-primary">RM <?php echo $courseList[$m]->amount;?></span>
            </h4>
          </div>
          <div class="d-md-flex">
          <button class="btn btn-primary mb-2 mr-3" onclick='buynow(<?php echo $courseList[$m]->id;?>)'>Buy Now</button>
        </div>
        </div>
      </div>

    <?php } ?>

    </div> <!-- END row-->
  </div> <!-- END container-->
</section>
   
   
   
   
<footer class="site-footer">
   <div class="footer-bottom bg-black-0_9 py-5 text-center">
    <div class="container">
      <p class="text-white-0_5 mb-0">&copy; 2020. All rights reserved</p>
    </div>
  </div>  <!-- END footer-bottom-->
</footer> 
<div class="scroll-top">
  <i class="ti-angle-up"></i>
</div>
     
    <script src="<?php echo BASE_PATH;?>website/js/vendors.bundle.js"></script>
    <script src="<?php echo BASE_PATH;?>website/js/scripts.js"></script>
  </body>
</html>


<script>
   function buynow(id)
    {
        $.get("/coursedetails/tempbuynow/"+id, function(data, status){
             console.log(data);
             parent.location= "<?php echo BASE_PATH;?>login";
         });
    }

  </script>