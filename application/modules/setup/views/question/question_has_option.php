<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Question</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

        <div class="page-container">

          <div>
            <h4 class="form-title">Question Details</h4>
          </div>

            <div class="form-container">
                <div class="row">
                <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool" id="id_pool" class="form-control" disabled>
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>" <?php
                                                                  if ($record->id == $question->id_pool) {
                                                                    echo "selected=selected";
                                                                  } ?>>
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Course <span class="text-danger">*</span></label>
                          <div class="col-sm-9">
                            <select name="id_course" id="id_course" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseList))
                            {
                                foreach ($courseList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>" 
                                        <?php 
                                        if($record->id == $question->id_course)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>


                </div>


                <div class="row">


                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Topic <span class="text-danger">*</span></label>
                          <div class="col-sm-9">
                            <select name="id_topic" id="id_topic" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($topicList))
                            {
                                foreach ($topicList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_topic)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                    

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Course Learning Objective <span class="text-danger">*</span></label>
                          <div class="col-sm-9">
                            <select name="id_course_learning_objective" id="id_course_learning_objective" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($courseLearningObjectList))
                            {
                                foreach ($courseLearningObjectList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_course_learning_objective)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                  </div>

                  <div class="row">

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Bloom Taxonomy <span class="text-danger">*</span></label>
                          <div class="col-sm-9">
                            <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($bloomTaxonomyList))
                            {
                                foreach ($bloomTaxonomyList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_bloom_taxonomy)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-3 col-form-label">Difficult Level <span class="text-danger">*</span></label>
                          <div class="col-sm-9">
                            <select name="id_difficult_level" id="id_difficult_level" class="form-control" disabled>
                            <option value="">Select</option>
                            <?php
                            if (!empty($difficultLevelList))
                            {
                                foreach ($difficultLevelList as $record)
                                {?>
                                    <option value="<?php echo $record->id;  ?>"
                                        <?php 
                                        if($record->id == $question->id_difficult_level)
                                        {
                                            echo "selected=selected";
                                        } ?>
                                        >
                                        <?php echo $record->code . " - " . $record->name;  ?>
                                    </option>
                            <?php
                                }
                            }
                            ?>
                            </select>
                          </div>
                        </div>
                    </div>

                  </div>


                  <div class="row">


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Upload Image

                         <?php 
                        if($question->image)
                        {
                        ?>
                            <br>
                            <a href="<?php echo '/assets/images/' . $question->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $question->image; ?>)" title="<?php echo $question->image; ?>">
                                            View
                                        </a>
                        <?php 

                        }else
                        {

                            echo "<br> <a title='No File'> No File Uploaded </a>";
                        }
                        ?>


                        </label>
                        <!-- <div class="col-sm-8">
                            <input type="file" class="form-control" id="image_file" name="image_file" >
                        </div> -->
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                            <?php if($question->status==1)
                            {
                                 echo "checked=checked";
                            };?>
                              disabled>
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                            <?php if($question->status==0)
                            {
                                 echo "checked=checked";
                            };?>
                            disabled>
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <!-- <button type="submit" class="btn btn-primary">Save</button> -->
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
                  </div>

                </div> 

            </div>  

            <div class="form-container">
            <h4 class="form-group-title"> Option Details</h4>          
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="clearfix">
                <ul class="nav nav-tabs offers-tab sub-tabs text-center" role="tablist" >
                    <li role="presentation" class="active" ><a href="#invoice" class="nav-link border rounded text-center"
                            aria-controls="invoice" aria-selected="true"
                            role="tab" data-toggle="tab">Options</a>
                    </li>
                </ul>

                
                <div class="tab-content offers-tab-content">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-4">




                        <form id="form_comitee" action="" method="post" enctype="multipart/form-data">


                                <br>

                                <div class="row">

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Option <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="option_description" name="option_description">
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <p>Is Correct Answer <span class='error-text'>*</span></p>
                                            <label class="radio-inline">
                                              <input type="radio" name="is_correct_answer" id="is_correct_answer" value="0" checked="checked"><span class="check-radio"></span> No
                                            </label>

                                            <label class="radio-inline">
                                              <input type="radio" name="is_correct_answer" id="is_correct_answer" value="1"><span class="check-radio"></span> Yes
                                            </label>
                                        </div>                         
                                    </div>



                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Upload Image</label>
                                            <input type="file" class="form-control" id="image" name="image" >
                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-primary btn-lg form-row-btn">Add</button>
                                    </div>
                                
                                </div>


                        </form>


                        <?php

                            if(!empty($questionHasOption))
                            {
                                ?>
                                <br>

                                <div class="form-container">
                                        <h4 class="form-group-title">Option Details</h4>

                                    

                                      <div class="custom-table">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Option</th>
                                                 <th class="text-center">Is Correct</th>
                                                 <th class="text-center">Image</th>
                                                 <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($questionHasOption);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $questionHasOption[$i]->option_description;?></td>
                                                <td style="text-align: center;"><?php if($questionHasOption[$i]->is_correct_answer == '1')
                                                  {
                                                    echo "Yes";
                                                  }
                                                  else
                                                  {
                                                    echo "No";
                                                  } 
                                                  ?></td>

                                                <td class="text-center">
                                                <?php 
                                                if($questionHasOption[$i]->image)
                                                {
                                                ?>
                                                <a href="<?php echo '/assets/images/' . $questionHasOption[$i]->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $questionHasOption[$i]->image; ?>)" title="<?php echo $questionHasOption[$i]->image; ?>">
                                                    View
                                                </a>
                                                <?php
                                                }else
                                                {
                                                    echo "<a title='No File '> No File Uploaded </a>";
                                                }
                                                ?>

                                                </td>


                                                <td class="text-center">
                                                <a onclick="deleteOption(<?php echo $questionHasOption[$i]->id; ?>)">Delete</a>
                                                </td>

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>




                </div>

            </div>
        </div> 


        </div>
</main>

<script>


    function deleteOption(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/setup/question/deleteOption/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_comitee").validate(
        {
            rules:
            {
                option_description:
                {
                    required: true
                },
                is_correct_answer:
                {
                    required: true
                }
            },
            messages:
            {
                option_description:
                {
                    required: "<p class='error-text'>Option Required</p>",
                },
                is_correct_answer:
                {
                    required: "<p class='error-text'>Select Is Correct / Not</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>