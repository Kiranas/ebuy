
<div class="py-2 bg-light-v2">
  <div class="container">
   <div class="row align-items-center">
     <div class="col-md-6">
       <h2>Privacy </h2>
     </div>
     <div class="col-md-6">
      <ol class="breadcrumb justify-content-md-end bg-transparent">  
        <li class="breadcrumb-item">
          <a href="/">Home</a>
        </li> 
        <li class="breadcrumb-item">
          <a href="#"> Privacy </a>
        </li>
      </ol>
     </div>
   </div>
  </div> 
</div>


  <section class="padding-y-10">
  <div class="container">
   <div class="row">
    
     <div class="col-12">
       
        <?php echo $aboutus[0]->privacy;?>
      </div>
     </div> <!-- END col-12 -->
     
   
   </div> <!-- END row-->  
  </div> <!-- END container-->
</section>

<script>

function deletetempcart(id) {
      $.get("/index/deletetemp/"+id, function(data, status){
             window.location.reload();
         });
}


function updatecart(id,qty) {
  $.get("/index/updateqty/"+id+"/"+qty, function(data, status){
             window.location.reload();
         });
}


  </script>
